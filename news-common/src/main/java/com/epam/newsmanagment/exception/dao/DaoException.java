package com.epam.newsmanagment.exception.dao;

/**
 * Wrapper for {@link java.sql.SQLException}
 */
public class DaoException extends Exception {
    public DaoException() {
    }

    public DaoException(Throwable cause) {
        super(cause);
    }

}
