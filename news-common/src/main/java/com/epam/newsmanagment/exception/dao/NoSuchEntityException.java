package com.epam.newsmanagment.exception.dao;

/**
 * This is wrapper for {@link Exception} which can be achieved
 * by getting entity from data model.
 */
public class NoSuchEntityException extends Exception {
    public NoSuchEntityException(Throwable cause) {
        super(cause);
    }
}
