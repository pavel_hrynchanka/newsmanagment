package com.epam.newsmanagment.exception.service;

/**
 * Wrapper for {@link java.sql.SQLException}
 */
public class ServiceException extends Exception {
    public ServiceException() {
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

}
