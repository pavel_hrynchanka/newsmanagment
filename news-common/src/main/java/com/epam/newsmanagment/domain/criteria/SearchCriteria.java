package com.epam.newsmanagment.domain.criteria;

import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.domain.bean.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * Search criteria is an object that contain parameters
 * for news searching.
 * Author and tag list are included in this object.
 * You can search news by author and any tags which
 * can be in the news.
 */
public class SearchCriteria implements Serializable {
    private static final long serialVersionUID = 31l;
    private Author author;
    private List<Tag> tagList;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchCriteria that = (SearchCriteria) o;

        if (!author.equals(that.author)) return false;
        return tagList.equals(that.tagList);

    }

    @Override
    public int hashCode() {
        int result = author.hashCode();
        result = 31 * result + tagList.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SearchCriteria{" +
                "author=" + author +
                ", tagList=" + tagList +
                '}';
    }
}
