package com.epam.newsmanagment.domain.to;

import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.bean.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * It is a container for news as transfer object.
 * It consists of author, tag list and comment list.
 */
public class NewsTO implements Serializable {
    private News news;
    private List<Author> author;
    private List<Tag> tagList;
    private List<Comment> commentList;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Author> getAuthor() {
        return author;
    }

    public void setAuthor(List<Author> author) {
        this.author = author;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsTO newsTO = (NewsTO) o;

        if (news != null ? !news.equals(newsTO.news) : newsTO.news != null) return false;
        if (author != null ? !author.equals(newsTO.author) : newsTO.author != null) return false;
        if (tagList != null ? !tagList.equals(newsTO.tagList) : newsTO.tagList != null) return false;
        return commentList != null ? commentList.equals(newsTO.commentList) : newsTO.commentList == null;

    }

    @Override
    public int hashCode() {
        int result = news != null ? news.hashCode() : 0;
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (tagList != null ? tagList.hashCode() : 0);
        result = 31 * result + (commentList != null ? commentList.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NewsTO{" +
                "news=" + news +
                ", author=" + author +
                ", tagList=" + tagList +
                ", commentList=" + commentList +
                '}';
    }
}
