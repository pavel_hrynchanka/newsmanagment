package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.CommentDao;
import com.epam.newsmanagment.dao.GenericDao;
import com.epam.newsmanagment.dao.NewsDao;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.exception.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link CommentDao}.
 */
@Repository
public class CommentDaoImpl implements CommentDao {
    private final static String INSERT_COMMENT_SQL = "INSERT INTO COMMENTS(COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) VALUES (COMMENT_SEQ.NEXTVAL,?,?,?)";
    private final static String SELECT_COMMENT_BY_ID_SQL = "SELECT COMMENT_ID,NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
    private final static String COUNT_ALL_COMMENT_SQL = "SELECT COUNT(COMMENT_ID) FROM COMMENTS";
    private final static String SELECT_ALL_COMMENT_SQL = "SELECT COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE FROM COMMENTS";
    private final static String UPDATE_COMMENT_SQL = "UPDATE COMMENTS SET COMMENT_TEXT = ? WHERE COMMENT_ID = ?";
    private final static String DELETE_COMMENT_BY_ID_SQL = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
    private final static String DELETE_COMMENTS_BY_NEWS_SQL = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
    private final static String SELECT_COMMENTS_BY_NEWS_SQL = "SELECT COMMENT_ID,NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS  WHERE NEWS_ID = ?";
    private final static String COMMENT_ID = "COMMENT_ID";

    @Autowired
    private DataSource dataSource;

    @Autowired
    private NewsDao newsDao;

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#create(Serializable)} using JDBC.
     */
    @Override
    public Long create(Comment comment) throws DaoException {
        Long commentId = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_COMMENT_SQL, new String[]{COMMENT_ID});
        ) {
            preparedStatement.setLong(1, comment.getNews().getNewsId());
            preparedStatement.setString(2, comment.getCommentText());
            preparedStatement.setTimestamp(3, comment.getCreationDate());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                commentId = resultSet.getLong(1);
                comment.setCommentId(commentId);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return commentId;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#getById(Long)} using JDBC.
     */
    @Override
    public Comment getById(Long commentId) throws DaoException {
        Comment comment = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_COMMENT_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, commentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                comment = buildComment(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return comment;
    }

    /**
     * Implementation of {@link GenericDao#getAll()} using JDBC.
     */
    @Override
    public List<Comment> getAll() throws DaoException {
        List<Comment> commentList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_ALL_COMMENT_SQL);
        ) {
            commentList = new ArrayList<Comment>();
            while (resultSet.next()) {
                Comment comment = buildComment(resultSet);
                commentList.add(comment);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return commentList;
    }

    /**
     * Implementation of {@link GenericDao#countAll()} using JDBC.
     */
    @Override
    public Long countAll() throws DaoException {
        Long quantity = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(COUNT_ALL_COMMENT_SQL);
        ) {
            quantity = new Long(0);
            if (resultSet.next()) {
                quantity = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return quantity;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#update(Serializable)} using JDBC.
     */
    @Override
    public void update(Comment comment) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_COMMENT_SQL)
        ) {
            preparedStatement.setString(1, comment.getCommentText());
            preparedStatement.setLong(2, comment.getCommentId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#delete(Long)} using JDBC.
     */
    @Override
    public void delete(Long commentId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COMMENT_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.CommentDao#getCommentList(Long)} using JDBC.
     */
    @Override
    public List<Comment> getCommentList(Long newsId) throws DaoException {
        List<Comment> commentList = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_COMMENTS_BY_NEWS_SQL);
        ) {
            commentList = new ArrayList<Comment>();
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Comment comment = buildComment(resultSet);
                commentList.add(comment);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return commentList;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.CommentDao#deleteCommentByNews(Long)} using JDBC.
     */
    @Override
    public void deleteCommentByNews(Long newsId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COMMENTS_BY_NEWS_SQL);
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    private Comment buildComment(ResultSet resultSet) throws DaoException {
        Comment comment = null;
        try {
            comment = new Comment();
            comment.setCommentId(resultSet.getLong(1));
            comment.setNews(newsDao.getById((Long) resultSet.getLong(2)));
            comment.setCommentText(resultSet.getString(3));
            comment.setCreationDate(resultSet.getTimestamp(4));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return comment;
    }
}
