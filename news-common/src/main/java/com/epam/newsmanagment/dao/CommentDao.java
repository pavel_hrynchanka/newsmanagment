package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.exception.dao.DaoException;

import java.util.List;

/**
 * Describe methods for {@link Comment} for operating
 * with database.
 */
public interface CommentDao extends GenericDao<Comment, Long> {
    /**
     * Find all comments of news.
     *
     * @param newsId is an id of comment which used for searching.
     * @return comment of the news.
     * @throws DaoException if was problem withe reading data from database
     */
    List<Comment> getCommentList(Long newsId) throws DaoException;

    /**
     * Delete comments of the news.
     *
     * @param newsId is an id of deleted news.
     * @throws DaoException if was problem withe reading data from database
     */
    void deleteCommentByNews(Long newsId) throws DaoException;
}
