package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.dao.DaoException;

import java.util.List;

/**
 * Describe methods for {@link Tag} for operating
 * with database.
 */

public interface TagDao extends GenericDao<Tag, Long> {
    /**
     * Find tags for the news.
     *
     * @param newsId is an id of news for which tags will be searched.
     * @return news tags.
     * @throws DaoException if was problem withe reading data from database.
     */
    List<Tag> getNewsTagList(Long newsId) throws DaoException;

}
