package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.exception.dao.DaoException;

import java.util.List;

/**
 * Describe methods for {@link News} for operating
 * with database.
 */
public interface NewsDao extends GenericDao<News, Long> {
    /**
     * Find news according to search criteria. It can find
     * news with similar tags. The tags quantity should be more
     * or equal than which was set. Author must be the same for all
     * news.
     *
     * @param searchCriteria is an parameters by which news searched.
     * @return all news which satisfy search criteria.
     * @throws DaoException if was problem withe reading data from database.
     */
    List<News> findNews(SearchCriteria searchCriteria) throws DaoException;

    List<News> findNews(SearchCriteria searchCriteria, int fromRow, int toRow) throws DaoException;

    News findNextNews(Long newsId) throws DaoException;

    News findPreviousNews(Long newsId) throws DaoException;

    List<News> getAll(int fromRow, int toRow) throws DaoException;

    /**
     * Add authors to news.
     *
     * @param newsId   is an id of news for which authors will be added.
     * @param authorId are authors id which news can have.
     * @return quantity of inserted rows.
     * @throws DaoException if was problem withe reading data from database.
     */
    Long addAuthorRelation(Long newsId, Long... authorId) throws DaoException;

    /**
     * Add tags to news.
     *
     * @param newsId is an id of news for which tags will be added.
     * @param tagId  are tags id which news can have.
     * @return quantity of inserted rows.
     * @throws DaoException if was problem withe reading data from database.
     */
    Long addTagRelation(Long newsId, Long... tagId) throws DaoException;

    /**
     * Delete authors from news.
     *
     * @param newsId is an id of news for which authors will be deleted.
     * @throws DaoException if was problem withe reading data from database.
     */
    void deleteAuthorRelation(Long newsId) throws DaoException;

    /**
     * Delete tags from news.
     *
     * @param newsId is an id of news for which tags will be deleted.
     * @throws DaoException if was problem withe reading data from database.
     */
    void deleteTagRelation(Long newsId) throws DaoException;

    void deleteTagRelation(Long newsId, Long tagId) throws DaoException;
}
