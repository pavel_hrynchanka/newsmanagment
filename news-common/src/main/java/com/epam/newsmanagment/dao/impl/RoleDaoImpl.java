package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.GenericDao;
import com.epam.newsmanagment.dao.RoleDao;
import com.epam.newsmanagment.domain.bean.Role;
import com.epam.newsmanagment.exception.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link RoleDao}.
 */
@Repository
public class RoleDaoImpl implements RoleDao {
    private final static String INSERT_ROLE_SQL = "INSERT INTO NM_ROLES(ROLE_ID,ROLE_NAME) VALUES (ROLE_SEQ.NEXTVAL,?)";
    private final static String SELECT_ROLE_BY_ID_SQL = "SELECT ROLE_ID,ROLE_NAME FROM NM_ROLES WHERE ROLE_ID = ?";
    private final static String SELECT_ALL_ROLE_SQL = "SELECT ROLE_ID,ROLE_NAME FROM NM_ROLES";
    private final static String COUNT_ALL_ROLE_SQL = "SELECT COUNT(ROLE_ID) FROM NM_ROLES";
    private final static String UPDATE_ROLE_SQL = "UPDATE NM_ROLES SET ROLE_NAME = ? WHERE ROLE_ID = ?";
    private final static String DELETE_ROLE_BY_ID_SQL = "DELETE FROM NM_ROLES WHERE ROLE_ID = ?";
    private final static String ROLE_ID = "ROLE_ID";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#create(Serializable)} using JDBC.
     */
    @Override
    public Long create(Role role) throws DaoException {
        Long roleId = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ROLE_SQL, new String[]{ROLE_ID});
        ) {
            preparedStatement.setString(1, role.getRoleName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                roleId = resultSet.getLong(1);
                role.setRoleId(roleId);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return roleId;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#getById(Long)} using JDBC.
     */
    @Override
    public Role getById(Long roleId) throws DaoException {
        Role role = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ROLE_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, roleId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                role = new Role();
                role.setRoleId(resultSet.getLong(1));
                role.setRoleName(resultSet.getString(2));
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return role;
    }

    /**
     * Implementation of {@link GenericDao#getAll()} using JDBC.
     */
    @Override
    public List<Role> getAll() throws DaoException {
        List<Role> roleList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_ALL_ROLE_SQL);
        ) {
            roleList = new ArrayList<Role>();
            while (resultSet.next()) {
                Role role = buildRole(resultSet);
                roleList.add(role);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return roleList;
    }

    /**
     * Implementation of {@link GenericDao#countAll()} using JDBC.
     */
    @Override
    public Long countAll() throws DaoException {
        Long quantity = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(COUNT_ALL_ROLE_SQL);
        ) {
            quantity = new Long(0);
            if (resultSet.next()) {
                quantity = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return quantity;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#update(Serializable)} using JDBC.
     */
    @Override
    public void update(Role role) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ROLE_SQL)
        ) {
            preparedStatement.setString(1, role.getRoleName());
            preparedStatement.setLong(2, role.getRoleId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#delete(Long)} using JDBC.
     */
    @Override
    public void delete(Long roleId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ROLE_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, roleId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    private Role buildRole(ResultSet resultSet) throws DaoException {
        Role role = null;
        try {
            role = new Role();
            role.setRoleId(resultSet.getLong(1));
            role.setRoleName(resultSet.getString(2));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return role;
    }
}
