package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.dao.DaoException;

/**
 * Describe methods for {@link User} for operating
 * with database.
 */
public interface UserDao extends GenericDao<User, Long> {
    /**
     * Check whether user was registered in system.
     *
     * @param userId is an id of checking user.
     * @return true of false according to was or wasn't user registered in system.
     * @throws DaoException if was problem withe reading data from database.
     */
    boolean checkUser(Long userId) throws DaoException;

    User getUser(String login, String password) throws DaoException;
}
