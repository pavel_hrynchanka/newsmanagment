package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.AuthorDao;
import com.epam.newsmanagment.dao.GenericDao;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link AuthorDao}.
 */
@Repository
public class AuthorDaoImpl implements AuthorDao {
    private final static String INSERT_AUTHOR_SQL = "INSERT INTO AUTHORS(AUTHOR_ID,AUTHOR_NAME,EXPIRED) VALUES(AUTHOR_SEQ.NEXTVAL,?,?)";
    private final static String SELECT_AUTHOR_BY_ID_SQL = "SELECT AUTHOR_ID,AUTHOR_NAME,EXPIRED FROM AUTHORS WHERE AUTHOR_ID = ?";
    private final static String SELECT_ALL_AUTHOR_SQL = "SELECT AUTHOR_ID,AUTHOR_NAME,EXPIRED FROM AUTHORS";
    private final static String COUNT_ALL_AUTHOR_SQL = "SELECT COUNT(AUTHOR_ID) FROM AUTHORS";
    private final static String UPDATE_AUTHOR_SQL = "UPDATE AUTHORS SET AUTHOR_NAME= ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
    private final static String DELETE_AUTHOR_BY_ID_SQL = "DELETE FROM AUTHORS WHERE AUTHOR_ID = ?";
    private final static String DELETE_AUTHOR_NEWS_BY_ID_SQL = "DELETE FROM NEWS_AUTHORS WHERE AUTHOR_ID = ?";
    private final static String SELECT_AUTHORS_BY_NEWS_SQL = "SELECT AT.AUTHOR_ID,AT.AUTHOR_NAME,AT.EXPIRED FROM AUTHORS AT JOIN NEWS_AUTHORS NAT ON AT.AUTHOR_ID=NAT.AUTHOR_ID WHERE NAT.NEWS_ID = ?";
    private final static String SELECT_AVAILABLE_AUTHORS_SQL = "SELECT AUTHOR_ID,AUTHOR_NAME,EXPIRED FROM AUTHORS WHERE EXPIRED IS NULL";
    private final static String AUTHOR_ID = "AUTHOR_ID";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#create(Serializable)} using JDBC.
     */
    @Override
    public Long create(Author author) throws DaoException {
        Long authorId = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_AUTHOR_SQL, new String[]{AUTHOR_ID});
        ) {
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setTimestamp(2, author.getExpired());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                authorId = resultSet.getLong(1);
                author.setAuthorId(authorId);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return authorId;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#getById(Long)} using JDBC.
     */
    @Override
    public Author getById(Long authorId) throws DaoException {
        Author author = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AUTHOR_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                author = buildAuthor(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return author;
    }

    /**
     * Implementation of {@link GenericDao#getAll()} using JDBC.
     */
    @Override
    public List<Author> getAll() throws DaoException {
        List<Author> authorList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_ALL_AUTHOR_SQL);
        ) {
            authorList = new ArrayList<Author>();
            while (resultSet.next()) {
                Author author = buildAuthor(resultSet);
                authorList.add(author);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return authorList;
    }

    /**
     * Implementation of {@link GenericDao#countAll()} using JDBC.
     */
    @Override
    public Long countAll() throws DaoException {
        Long quantity = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(COUNT_ALL_AUTHOR_SQL);
        ) {
            quantity = new Long(0);
            if (resultSet.next()) {
                quantity = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return quantity;
    }

    /**
     * Implementation of {@link GenericDao#update(Serializable)} using JDBC.
     */
    @Override
    public void update(Author author) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_AUTHOR_SQL)
        ) {
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setTimestamp(2, author.getExpired());
            preparedStatement.setLong(3, author.getAuthorId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link GenericDao#delete(Long)} using JDBC.
     */
    @Override
    public void delete(Long authorId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_AUTHOR_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link AuthorDao#getAuthorList(Long)} using JDBC.
     */
    @Override
    public List<Author> getAuthorList(Long newsId) throws DaoException {
        List<Author> authorList = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_AUTHORS_BY_NEWS_SQL);
        ) {
            authorList = new ArrayList<>();
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Author author = buildAuthor(resultSet);
                authorList.add(author);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return authorList;
    }

    /**
     * Implementation of {@link AuthorDao#getAvailableAuthorList()} using JDBC.
     */
    @Override
    public List<Author> getAvailableAuthorList() throws DaoException {
        List<Author> authorList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_AVAILABLE_AUTHORS_SQL);
        ) {
            authorList = new ArrayList<Author>();
            while (resultSet.next()) {
                Author author = buildAuthor(resultSet);
                authorList.add(author);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return authorList;
    }

    /**
     * Implementation of {@link AuthorDao#deleteAuthorNewsRelation(Long)} using JDBC.
     */
    @Override
    public void deleteAuthorNewsRelation(Long authorId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_AUTHOR_NEWS_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    private Author buildAuthor(ResultSet resultSet) throws DaoException {
        Author author = null;
        try {
            author = new Author();
            author.setAuthorId(resultSet.getLong(1));
            author.setAuthorName(resultSet.getString(2));
            author.setExpired(resultSet.getTimestamp(3));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return author;
    }
}
