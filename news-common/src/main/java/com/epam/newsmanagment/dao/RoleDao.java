package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.domain.bean.Role;

/**
 * * Describe methods for {@link Role} for operating
 * with database.
 */
public interface RoleDao extends GenericDao<Role, Long> {
}
