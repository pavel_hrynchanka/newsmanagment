package com.epam.newsmanagment.dao.util;


public class PaginationHelper {
    private int recordsQuantity;
    private int recordsPerPage;

    public PaginationHelper(int recordsQuantity, int recordsPerPage) {
        this.recordsQuantity = recordsQuantity;
        this.recordsPerPage = recordsPerPage;
    }

    public int countPages() {
        return (int) (Math.ceil((double) recordsQuantity / recordsPerPage));
    }

    public int getFromBound(int pageNumber) {
        return recordsPerPage * (pageNumber - 1) + 1;
    }

    public int getToBound(int pageNumber) {
        return pageNumber * recordsPerPage;
    }
}
