package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.GenericDao;
import com.epam.newsmanagment.dao.NewsDao;
import com.epam.newsmanagment.dao.util.QueryBuilder;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.exception.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link NewsDao}.
 */
@Repository
public class NewsDaoImpl implements NewsDao {
    private final static String INSERT_NEWS_SQL = "INSERT INTO NEWS(NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE) VALUES(NEWS_SEQ.NEXTVAL,?,?,?,?,?)";
    private final static String SELECT_NEWS_BY_ID_SQL = "SELECT NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
    private final static String SELECT_ALL_NEWS_SQL = "SELECT NS.NEWS_ID,NS.TITLE,NS.SHORT_TEXT,NS.FULL_TEXT,NS.CREATION_DATE,NS.MODIFICATION_DATE,NVL(NQ.QT,0) QT FROM NEWS NS " +
            "LEFT JOIN (SELECT NEWS_ID,COUNT(NEWS_ID) QT FROM COMMENTS GROUP BY NEWS_ID ORDER BY QT DESC) NQ ON NS.NEWS_ID = NQ.NEWS_ID ORDER BY(QT) DESC";
    private final static String SELECT_ALL_NEWS_PAGING_SQL = "SELECT NS.NEWS_ID,NS.TITLE,NS.SHORT_TEXT,NS.FULL_TEXT,NS.CREATION_DATE,NS.MODIFICATION_DATE,NVL(NQ.QT,0) QT " +
            "FROM (SELECT NEWS.*, ROWNUM RN FROM NEWS) NS " +
            "LEFT JOIN " +
            "(SELECT NEWS_ID,COUNT(NEWS_ID) QT FROM COMMENTS GROUP BY NEWS_ID ORDER BY QT DESC) NQ " +
            "ON NS.NEWS_ID = NQ.NEWS_ID " +
            "WHERE NS.RN BETWEEN ? AND ? " +
            "ORDER BY(QT) DESC";
    private final static String FIND_PREV_NEWS = "SELECT NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE " +
            "FROM NEWS NS JOIN " +
            "(SELECT NP.PREV_NEWS FROM " +
            "(SELECT LAG(NEWS_ID,1,0) OVER(ORDER BY NEWS_ID) PREV_NEWS,NEWS_ID CURRENT_ID,LEAD(NEWS_ID,1,0) OVER(ORDER BY NEWS_ID) NEXT_NEWS " +
            "FROM NEWS ORDER BY NEWS_ID) NP " +
            "WHERE NP.CURRENT_ID = ?) P " +
            "ON P.PREV_NEWS=NS.NEWS_ID";
    private final static String FIND_NEXT_NEWS = "SELECT NEWS_ID,TITLE,SHORT_TEXT,FULL_TEXT,CREATION_DATE,MODIFICATION_DATE " +
            "FROM NEWS NS JOIN " +
            "(SELECT NP.NEXT_NEWS FROM " +
            "(SELECT LAG(NEWS_ID,1,0) OVER(ORDER BY NEWS_ID) PREV_NEWS,NEWS_ID CURRENT_ID,LEAD(NEWS_ID,1,0) OVER(ORDER BY NEWS_ID) NEXT_NEWS " +
            "FROM NEWS ORDER BY NEWS_ID) NP " +
            "WHERE NP.CURRENT_ID = ?) N " +
            "ON N.NEXT_NEWS=NS.NEWS_ID";
    private final static String COUNT_ALL_NEWS_SQL = "SELECT COUNT(NEWS_ID) FROM NEWS";
    private final static String UPDATE_NEWS_SQL = "UPDATE NEWS SET TITLE=?,SHORT_TEXT=?,FULL_TEXT=?,MODIFICATION_DATE=? WHERE NEWS_ID = ?";
    private final static String DELETE_NEWS_BY_ID_SQL = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private final static String DELETE_TAGS_FROM_NEWS_SQL = "DELETE FROM NEWS_AUTHORS WHERE NEWS_ID = ?";
    private final static String DELETE_AUTHORS_FROM_NEWS_SQL = "DELETE FROM NEWS_TAGS WHERE NEWS_ID = ?";
    private final static String DELETE_TAG_WIRE_SQL = "DELETE FROM NEWS_TAGS WHERE NEWS_ID = ? AND TAG_ID = ?";
    private final static String ADD_AUTHOR_TO_NEWS_SQL = "INSERT INTO NEWS_AUTHORS(NEWS_ID,AUTHOR_ID) VALUES(?,?)";
    private final static String ADD_TAG_TO_NEWS_SQL = "INSERT INTO NEWS_TAGS(NEWS_ID,TAG_ID) VALUES(?,?)";
    private final static String NEWS_ID = "NEWS_ID";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#create(Serializable)} using JDBC.
     */
    @Override
    public Long create(News news) throws DaoException {
        Long newsId = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_NEWS_SQL, new String[]{NEWS_ID});
        ) {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                newsId = resultSet.getLong(1);
                news.setNewsId(newsId);
            }

        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return newsId;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#getById(Long)} using JDBC.
     */
    @Override
    public News getById(Long newsId) throws DaoException {
        return findByIdWithPreparedStatement(newsId, SELECT_NEWS_BY_ID_SQL);
    }

    /**
     * Implementation of {@link GenericDao#getAll()} using JDBC.
     */
    @Override
    public List<News> getAll() throws DaoException {
        return buildNewsList(SELECT_ALL_NEWS_SQL);
    }

    @Override
    public List<News> getAll(int fromRow, int toRow) throws DaoException {
        return buildNewsListPrepared(fromRow, toRow);
    }

    /**
     * Implementation of {@link GenericDao#countAll()} using JDBC.
     */
    @Override
    public Long countAll() throws DaoException {
        Long quantity = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(COUNT_ALL_NEWS_SQL);
        ) {
            quantity = new Long(0);
            if (resultSet.next()) {
                quantity = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return quantity;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#update(Serializable)} using JDBC.
     */
    @Override
    public void update(News news) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS_SQL)
        ) {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setDate(4, news.getModificationDate());
            preparedStatement.setLong(5, news.getNewsId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#delete(Long)} using JDBC.
     */
    @Override
    public void delete(Long newsId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.NewsDao#findNews(SearchCriteria)} using JDBC.
     */
    @Override
    public List<News> findNews(SearchCriteria searchCriteria) throws DaoException {
        String querySql = QueryBuilder.buildQuery(searchCriteria);
        return buildNewsList(querySql);
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.NewsDao#findNews(SearchCriteria, int, int)} using JDBC.
     */
    @Override
    public List<News> findNews(SearchCriteria searchCriteria, int fromRow, int toRow) throws DaoException {
        String querySql = QueryBuilder.buildQuery(searchCriteria, fromRow, toRow);
        return buildNewsList(querySql);
    }

    @Override
    public News findNextNews(Long newsId) throws DaoException {
        return findByIdWithPreparedStatement(newsId, FIND_NEXT_NEWS);
    }

    @Override
    public News findPreviousNews(Long newsId) throws DaoException {
        return findByIdWithPreparedStatement(newsId, FIND_PREV_NEWS);
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.NewsDao#addAuthorRelation(Long, Long...)} using JDBC.
     */
    @Override
    public Long addAuthorRelation(Long newsId, Long... authorId) throws DaoException {
        return addEntityRelation(ADD_AUTHOR_TO_NEWS_SQL, newsId, authorId);
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.NewsDao#addTagRelation(Long, Long...)} using JDBC.
     */
    @Override
    public Long addTagRelation(Long newsId, Long... tagId) throws DaoException {
        return addEntityRelation(ADD_TAG_TO_NEWS_SQL, newsId, tagId);
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.NewsDao#deleteAuthorRelation(Long)} using JDBC.
     */
    @Override
    public void deleteAuthorRelation(Long newsId) throws DaoException {
        deleteEntityRelation(newsId, DELETE_AUTHORS_FROM_NEWS_SQL);
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.NewsDao#deleteTagRelation(Long)} using JDBC.
     */
    @Override
    public void deleteTagRelation(Long newsId) throws DaoException {
        deleteEntityRelation(newsId, DELETE_TAGS_FROM_NEWS_SQL);
    }

    @Override
    public void deleteTagRelation(Long newsId, Long tagId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TAG_WIRE_SQL);
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.setLong(2, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    private void deleteEntityRelation(Long newsId, String sql) throws DaoException {
        try (
                Connection conection = dataSource.getConnection();
                PreparedStatement preparedStatement = conection.prepareStatement(sql);
        ) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }


    private Long addEntityRelation(String sql, Long newsId, Long... entityId) throws DaoException {
        Long rowsInserted = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            for (int i = 0; i < entityId.length; i++) {
                preparedStatement.setLong(1, newsId);
                preparedStatement.setLong(2, entityId[i]);
                preparedStatement.addBatch();
            }
            rowsInserted = new Long(preparedStatement.executeBatch().length);
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return rowsInserted;
    }

    private News buildNews(ResultSet resultSet) throws DaoException {
        News news = null;
        try {
            news = new News();
            news.setNewsId(resultSet.getLong(1));
            news.setTitle(resultSet.getString(2));
            news.setShortText(resultSet.getString(3));
            news.setFullText(resultSet.getString(4));
            news.setCreationDate(resultSet.getTimestamp(5));
            news.setModificationDate(resultSet.getDate(6));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return news;
    }

    private List<News> buildNewsList(String querySql) throws DaoException {
        List<News> newsList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(querySql);
        ) {
            newsList = new ArrayList<News>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return newsList;
    }

    private List<News> buildNewsListPrepared(int from, int to) throws DaoException {
        List<News> newsList = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_NEWS_PAGING_SQL);
        ) {
            preparedStatement.setInt(1, from);
            preparedStatement.setInt(2, to);
            ResultSet resultSet = resultSet = preparedStatement.executeQuery();
            newsList = new ArrayList<News>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                newsList.add(news);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return newsList;
    }

    private News findByIdWithPreparedStatement(Long newsId, String preparedQuery) throws DaoException {
        News news = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(preparedQuery);
        ) {
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                news = buildNews(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return news;
    }
}
