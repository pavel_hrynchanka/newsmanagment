package com.epam.newsmanagment.dao.util;

import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;

import java.util.List;

/**
 * Search criteria use this class to build dynamic sql queryByAuthorAndTags.
 */
public class QueryBuilder {
    private static String finalQuery;
    private static String queryByAuthorAndTags = "SELECT NS.* FROM NEWS NS JOIN (" +
            "SELECT NA.NEWS_ID,COUNT(NT.TAG_ID) QT FROM NEWS_AUTHORS NA " +
            "JOIN NEWS_TAGS NT ON NT.NEWS_ID = NA.NEWS_ID " +
            "WHERE NA.AUTHOR_ID = #AUTHOR " +
            "AND NA.NEWS_ID IN (SELECT DISTINCT NT.NEWS_ID FROM NEWS_TAGS NT WHERE NT.TAG_ID IN (#TAGS) ) " +
            "GROUP BY NA.NEWS_ID " +
            "HAVING COUNT(DISTINCT NT.TAG_ID)>=#QUANTITY ) NI " +
            "ON NS.NEWS_ID = NI.NEWS_ID";
    private static String queryByAuthor = "SELECT NS.* FROM NEWS NS " +
            "JOIN (SELECT NA.NEWS_ID FROM NEWS_AUTHORS NA WHERE NA.AUTHOR_ID = #AUTHOR) NI " +
            "ON NS.NEWS_ID = NI.NEWS_ID";
    private static String queryByAuthorAndTagsWithPaging = "SELECT NRR.* FROM ( " +
            "SELECT NR.*, ROWNUM RN FROM " +
            "(SELECT NS.* FROM NEWS NS JOIN " +
            "(SELECT NA.NEWS_ID,COUNT(NT.TAG_ID) QT FROM NEWS_AUTHORS NA " +
            "JOIN NEWS_TAGS NT ON NT.NEWS_ID = NA.NEWS_ID " +
            "WHERE NA.AUTHOR_ID = #AUTHOR " +
            "AND NA.NEWS_ID IN (SELECT DISTINCT NT.NEWS_ID FROM NEWS_TAGS NT WHERE NT.TAG_ID IN (#TAGS)) " +
            "GROUP BY NA.NEWS_ID " +
            "HAVING COUNT(DISTINCT NT.TAG_ID)>=#QUANTITY) NI " +
            "ON NS.NEWS_ID = NI.NEWS_ID " +
            ") NR) NRR " +
            "WHERE NRR.RN BETWEEN #FROM AND #TO";
    private static String queryByAuthorWithPaging = "SELECT ARR.*, ROWNUM RN FROM ( " +
            "SELECT AR.*, ROWNUM RN FROM ( " +
            "SELECT NS.* FROM NEWS NS " +
            "JOIN (SELECT NA.NEWS_ID FROM NEWS_AUTHORS NA WHERE NA.AUTHOR_ID = #AUTHOR) NI " +
            "ON NS.NEWS_ID = NI.NEWS_ID) AR) ARR " +
            "WHERE ARR.RN BETWEEN #FROM AND #TO";
    private final static String COMMA = ", ";
    private final static String AUTHOR = "#AUTHOR";
    private final static String TAGS = "#TAGS";
    private final static String QUANTITY = "#QUANTITY";
    private final static String PAGE_FROM = "#FROM";
    private final static String PAGE_TO = "#TO";

    private QueryBuilder() {
    }

    /**
     * Build quiry according to search criteria.
     * It uses base queryByAuthorAndTags. Query is constructed dynamically.
     *
     * @param searchCriteria is and parameters for searching.
     * @return sql queryByAuthorAndTags.
     */

    public static String buildQuery(SearchCriteria searchCriteria) {
        return compose(searchCriteria);
    }

    public static String buildQuery(SearchCriteria searchCriteria, int fromRow, int toRow) {
        return compose(searchCriteria, fromRow, toRow);
    }

    private static String compose(SearchCriteria searchCriteria) {
        if (!searchCriteria.getTagList().isEmpty()) {
            return composeAuthorAndTags(searchCriteria);
        } else {
            return composeAuthor(searchCriteria);
        }
    }

    private static String compose(SearchCriteria searchCriteria, int from, int to) {
        if (!searchCriteria.getTagList().isEmpty()) {
            return composeAuthorAndTags(searchCriteria, from, to);
        } else {
            return composeAuthor(searchCriteria, from, to);
        }
    }

    private static String composeAuthorAndTags(SearchCriteria searchCriteria) {
        finalQuery = queryByAuthorAndTags;
        finalQuery = addAuthor(searchCriteria.getAuthor());
        finalQuery = addTagQuantity(searchCriteria.getTagList().size());
        finalQuery = addTags(searchCriteria.getTagList());
        return addAuthor(searchCriteria.getAuthor());
    }

    private static String composeAuthorAndTags(SearchCriteria searchCriteria, int from, int to) {
        finalQuery = queryByAuthorAndTagsWithPaging;
        finalQuery = addAuthor(searchCriteria.getAuthor());
        finalQuery = addTagQuantity(searchCriteria.getTagList().size());
        finalQuery = addTags(searchCriteria.getTagList());
        finalQuery = addPagination(from, to);
        return addAuthor(searchCriteria.getAuthor());
    }

    private static String composeAuthor(SearchCriteria searchCriteria) {
        finalQuery = queryByAuthor;
        return addAuthor(searchCriteria.getAuthor());
    }

    private static String composeAuthor(SearchCriteria searchCriteria, int from, int to) {
        finalQuery = queryByAuthorWithPaging;
        finalQuery = addPagination(from, to);
        return addAuthor(searchCriteria.getAuthor());
    }

    private static String addAuthor(Author author) {
        return finalQuery.replace(AUTHOR, author.getAuthorId().toString());
    }

    private static String addTags(List<Tag> tagList) {
        StringBuilder tagsString = new StringBuilder();
        for (int i = 0; i < tagList.size(); ++i) {
            tagsString.append(tagList.get(i).getTagId().toString());
            if (i != tagList.size() - 1) {
                tagsString.append(COMMA);
            }
        }
        return finalQuery.replace(TAGS, tagsString.toString());
    }

    private static String addTagQuantity(Integer tagQuantity) {
        return finalQuery.replace(QUANTITY, tagQuantity.toString());
    }

    private static String addPagination(int from, int to) {
        finalQuery = finalQuery.replace(PAGE_FROM, String.valueOf(from));
        return finalQuery.replace(PAGE_TO, String.valueOf(to));
    }
}
