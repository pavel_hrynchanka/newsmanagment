package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.GenericDao;
import com.epam.newsmanagment.dao.RoleDao;
import com.epam.newsmanagment.dao.UserDao;
import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link UserDao}.
 */
@Repository
public class UserDaoImpl implements UserDao {
    private final static String INSERT_USER_SQL = "INSERT INTO NM_USERS(USER_ID,USER_NAME,USER_LOGIN,USER_PASSWORD,ROLE_ID) VALUES (USER_SEQ.NEXTVAL,?,?,?,?)";
    private final static String SELECT_USER_BY_ID_SQL = "SELECT USER_ID,USER_NAME,USER_LOGIN,USER_PASSWORD,ROLE_ID FROM NM_USERS WHERE USER_ID = ?";
    private final static String SELECT_ALL_USER_SQL = "SELECT USER_ID,USER_NAME,USER_LOGIN,USER_PASSWORD,ROLE_ID FROM NM_USERS";
    private final static String DELETE_USER_BY_ID_SQL = "DELETE FROM NM_USERS WHERE USER_ID = ?";
    private final static String UPDATE_USER_SQL = "UPDATE NM_USERS SET USER_PASSWORD=?, ROLE_ID=? WHERE USER_ID = ?";
    private final static String COUNT_ALL_USER_SQL = "SELECT COUNT(USER_ID) FROM NM_USERS";
    private final static String CHECK_USER_BY_ID_SQL = "SELECT USER_ID FROM NM_USERS WHERE USER_ID = ?";
    private final static String SELECT_USER_BY_LOGIN_AND_PWD_SQL = "SELECT USER_ID,USER_NAME,USER_LOGIN,USER_PASSWORD,ROLE_ID FROM NM_USERS US WHERE US.USER_LOGIN=? AND US.USER_PASSWORD=?";
    private final static String USER_ID = "USER_ID";

    @Autowired
    private DataSource dataSource;

    @Autowired
    private RoleDao roleDao;

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#create(Serializable)} using JDBC.
     */
    @Override
    public Long create(User user) throws DaoException {
        Long userId = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL, new String[]{USER_ID});
        ) {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getUserLogin());
            preparedStatement.setString(3, user.getUserPassword());
            preparedStatement.setLong(4, user.getRole().getRoleId());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                userId = resultSet.getLong(1);
                user.setUserId(userId);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return userId;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#getById(Long)} using JDBC.
     */
    @Override
    public User getById(Long userId) throws DaoException {
        User user = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = buildUser(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return user;
    }

    /**
     * Implementation of {@link GenericDao#getAll()} using JDBC.
     */
    @Override
    public List<User> getAll() throws DaoException {
        List<User> userList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_ALL_USER_SQL);
        ) {
            userList = new ArrayList<User>();
            while (resultSet.next()) {
                User user = buildUser(resultSet);
                userList.add(user);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return userList;
    }

    /**
     * Implementation of {@link GenericDao#countAll()} using JDBC.
     */
    @Override
    public Long countAll() throws DaoException {
        Long quantity = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(COUNT_ALL_USER_SQL);
        ) {
            quantity = new Long(0);
            if (resultSet.next()) {
                quantity = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return quantity;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#update(Serializable)} using JDBC.
     */
    @Override
    public void update(User user) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_SQL)
        ) {
            preparedStatement.setString(1, user.getUserPassword());
            preparedStatement.setLong(2, user.getRole().getRoleId());
            preparedStatement.setLong(3, user.getUserId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#delete(Long)} using JDBC.
     */
    @Override
    public void delete(Long userId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.UserDao#checkUser(Long)} using JDBC.
     */
    @Override
    public boolean checkUser(Long userId) throws DaoException {
        boolean flag = false;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CHECK_USER_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                flag = true;
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return flag;
    }

    @Override
    public User getUser(String login, String password) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_LOGIN_AND_PWD_SQL);
        ) {
            User user = null;
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return buildUser(resultSet);
            }
            return user;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    private User buildUser(ResultSet resultSet) throws DaoException {
        User user = null;
        try {
            user = new User();
            user.setUserId(resultSet.getLong(1));
            user.setUserName(resultSet.getString(2));
            user.setUserLogin(resultSet.getString(3));
            user.setUserPassword(resultSet.getString(4));
            user.setRole(roleDao.getById((Long) resultSet.getLong(5)));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return user;
    }
}
