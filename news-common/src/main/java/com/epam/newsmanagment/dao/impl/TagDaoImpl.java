package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.GenericDao;
import com.epam.newsmanagment.dao.TagDao;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.dao.DaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link TagDao}.
 */
@Repository
public class TagDaoImpl implements TagDao {
    private final static String INSERT_TAG_SQL = "INSERT INTO TAGS(TAG_ID,TAG_NAME) VALUES (TAG_SEQ.NEXTVAL,?)";
    private final static String SELECT_TAG_BY_ID_SQL = "SELECT TAG_ID,TAG_NAME FROM TAGS WHERE TAG_ID = ?";
    private final static String SELECT_ALL_TAG_SQL = "SELECT TAG_ID,TAG_NAME FROM TAGS ORDER BY TAG_ID ASC";
    private final static String DELETE_TAG_BY_ID_SQL = "DELETE FROM TAGS WHERE TAG_ID = ?";
    private final static String UPDATE_TAG_SQL = "UPDATE TAGS SET TAG_NAME = ? WHERE TAG_ID = ?";
    private final static String COUNT_ALL_TAG_SQL = "SELECT COUNT(TAG_ID) FROM TAGS";
    private final static String SELECT_TAGS_BY_NEWS_SQL = "SELECT TG.TAG_ID,TG.TAG_NAME FROM TAGS TG JOIN NEWS_TAGS NT ON NT.TAG_ID = TG.TAG_ID WHERE NT.NEWS_ID=?";
    private final static String TAG_ID = "TAG_ID";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#create(Serializable)} using JDBC.
     */
    @Override
    public Long create(Tag tag) throws DaoException {
        Long tagId = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TAG_SQL, new String[]{TAG_ID});
        ) {
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                tagId = resultSet.getLong(1);
                tag.setTagId(tagId);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return tagId;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#getById(Long)} using JDBC.
     */
    @Override
    public Tag getById(Long tagId) throws DaoException {
        Tag tag = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAG_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, tagId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                tag = buildTag(resultSet);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return tag;
    }

    /**
     * Implementation of {@link GenericDao#getAll()} using JDBC.
     */
    @Override
    public List<Tag> getAll() throws DaoException {
        List<Tag> tagList = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(SELECT_ALL_TAG_SQL);
        ) {
            tagList = new ArrayList<Tag>();
            while (resultSet.next()) {
                Tag tag = buildTag(resultSet);
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return tagList;
    }

    /**
     * Implementation of {@link GenericDao#countAll()} using JDBC.
     */
    @Override
    public Long countAll() throws DaoException {
        Long quantity = null;
        try (
                Connection connection = dataSource.getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(COUNT_ALL_TAG_SQL);
        ) {
            quantity = new Long(0);
            if (resultSet.next()) {
                quantity = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return quantity;
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#update(Serializable)} using JDBC.
     */
    @Override
    public void update(Tag tag) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TAG_SQL)
        ) {
            preparedStatement.setString(1, tag.getTagName());
            preparedStatement.setLong(2, tag.getTagId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
    /**
     * Implementation of {@link com.epam.newsmanagment.dao.GenericDao#delete(Long)} using JDBC.
     */
    @Override
    public void delete(Long tagId) throws DaoException {
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TAG_BY_ID_SQL);
        ) {
            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }
    /**
     * Implementation of {@link com.epam.newsmanagment.dao.TagDao#getNewsTagList(Long)} using JDBC.
     */
    @Override
    public List<Tag> getNewsTagList(Long newsId) throws DaoException {
        List<Tag> tagList = null;
        try (
                Connection connection = dataSource.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TAGS_BY_NEWS_SQL)
        ) {
            tagList = new ArrayList<Tag>();
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Tag tag = buildTag(resultSet);
                tagList.add(tag);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return tagList;
    }

    private Tag buildTag(ResultSet resultSet) throws DaoException {
        Tag tag = null;
        try {
            tag = new Tag();
            tag.setTagId(resultSet.getLong(1));
            tag.setTagName(resultSet.getString(2));
        } catch (SQLException e) {
            throw new DaoException(e);
        }
        return tag;
    }
}