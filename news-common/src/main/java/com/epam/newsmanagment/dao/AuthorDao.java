package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.dao.DaoException;

import java.util.List;

/**
 * Describe methods for {@link Author} for operating
 * with database.
 */
public interface AuthorDao extends GenericDao<Author, Long> {
    /**
     * Find authors of news by news id.
     *
     * @param newsId is an id of searched news.
     * @return all authors of news.
     * @throws DaoException if was problem withe reading data from database
     */
    List<Author> getAuthorList(Long newsId) throws DaoException;

    /**
     * Find only available authors.
     *
     * @return available authors.
     * @throws DaoException if was problem withe reading data from database.
     */
    List<Author> getAvailableAuthorList() throws DaoException;

    /**
     * Delete from link table relation between news and authors
     *
     * @param authorId is an id of author which relation should be deleted.
     * @throws DaoException if was problem withe reading data from database.
     */
    void deleteAuthorNewsRelation(Long authorId) throws DaoException;

}
