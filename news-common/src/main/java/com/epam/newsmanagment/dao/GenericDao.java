package com.epam.newsmanagment.dao;

import com.epam.newsmanagment.exception.dao.DaoException;

import java.io.Serializable;
import java.util.List;

/**
 * Provides basic crud operations for all entities {@link Entity}.
 *
 * @param <Entity> generic type of an entity.
 * @param <PK>     primary key of an entity.
 */
public interface GenericDao<Entity extends Serializable, PK extends Long> {
    /**
     * Create {@link Entity} as persist record in database.
     *
     * @param transferObject is an entity which should be created in database.
     * @return id of inserted record.
     * @throws DaoException if some problems rise at work with database.
     */
    PK create(Entity transferObject) throws DaoException;

    /**
     * Find record by {@link Entity} id in database
     *
     * @param primaryKey is an id of {@link Entity}.
     * @return {@link Entity} which persisted in database.
     * @throws DaoException if were problems with reading
     *                           record from database.
     */
    Entity getById(PK primaryKey) throws DaoException;

    /**
     * Find all persisted {@link Entity} in database.
     *
     * @return list of all records of {@link Entity}
     * @throws DaoException will occur if invoke some problems with
     *                           reading from database.
     */
    List<Entity> getAll() throws DaoException;

    /**
     * Count all persisted {@link Entity} in database.
     *
     * @return quantity of all {@link Entity}.
     * @throws DaoException will occur if invoke some problems with
     *                           reading from database.
     */
    PK countAll() throws DaoException;

    /**
     * Update persisted {@link Entity} in database.
     * It depends on {@link Entity} which attributes can be updated.
     *
     * @param transferObject represents {@link Entity} which should be updated.
     * @throws DaoException will occur if invoke some problems with
     *                           reading from database.
     */
    void update(Entity transferObject) throws DaoException;

    /**
     * Delete {@link Entity} from database.
     *
     * @param primaryKey is an id of {@link Entity}.
     * @throws DaoException will occur if invoke some problems with
     *                           reading from database.
     */
    void delete(PK primaryKey) throws DaoException;
}
