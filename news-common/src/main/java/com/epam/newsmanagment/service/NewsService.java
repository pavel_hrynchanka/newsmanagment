package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

import java.util.List;

/**
 * This is service which provide api for operation with {@link com.epam.newsmanagment.dao.NewsDao}
 */
public interface NewsService extends GenericService<News, Long> {
    /**
     * It asks {@link com.epam.newsmanagment.dao.NewsDao} to find news according
     * to search criteria.
     *
     * @param searchCriteria is an parameter for search.
     * @return news which satisfied search criteria.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<News> findNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    List<News> findNewsBySearchCriteria(SearchCriteria searchCriteria, int fromNews, int toNews) throws ServiceException;

    List<News> findAll(int fromNews, int toNews) throws ServiceException;

    News findNextNews(Long newsId) throws ServiceException;

    News findPreviousNews(Long newsId) throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.NewsDao} to add authors to news.
     *
     * @param newsId    is an id of news for which authors will be added.
     * @param authorsId are authors which will be added to news.
     * @return quantity of inserted rows.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    Long addAuthorsToNews(Long newsId, Long... authorsId) throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.NewsDao} to add tags to news.
     *
     * @param newsId is an id of news for which tags will be added.
     * @param tagsId are tags which will be added to news.
     * @return quantity of inserted rows.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    Long addTagsToNews(Long newsId, Long... tagsId) throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.NewsDao} to delete all news authors.
     *
     * @param newsId is an id of news which authors will be deleted.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void deleteAuthorsFromNews(Long newsId) throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.NewsDao} to delete all news tags.
     *
     * @param newsId is an id of news which tags will be deleted.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void deleteTagsFromNews(Long newsId) throws ServiceException;

    void deleteTagsWireFromNews(Long newsId, Long... tagsId) throws ServiceException;
}
