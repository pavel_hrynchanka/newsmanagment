package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.Role;

/**
 * This is service which provide api for operation with {@link com.epam.newsmanagment.dao.RoleDao}
 */
public interface RoleService extends GenericService<Role, Long> {
}
