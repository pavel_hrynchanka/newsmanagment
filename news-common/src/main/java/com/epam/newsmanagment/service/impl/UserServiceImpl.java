package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.UserDao;
import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.GenericService;
import com.epam.newsmanagment.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link UserService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class UserServiceImpl implements UserService {
    private final static Logger LOGGER = LogManager.getLogger(UserService.class);
    @Autowired
    private UserDao userDao;

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#create(Object)}.
     */
    @Override
    public Long create(User user) throws ServiceException {
        try {
            return userDao.create(user);
        } catch (DaoException e) {
            LOGGER.error("Failed to create user " + user, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#findById(Object)}.
     */
    @Override
    public User findById(Long userId) throws ServiceException {
        try {
            return userDao.getById(userId);
        } catch (DaoException e) {
            LOGGER.error("Can't find user with id " + userId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findAll()}.
     */
    @Override
    public List<User> findAll() throws ServiceException {
        try {
            return userDao.getAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to find all users", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public Long countAll() throws ServiceException {
        try {
            return userDao.countAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to count all", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#update(Object)}.
     */
    @Override
    public void update(User user) throws ServiceException {
        try {
            userDao.update(user);
        } catch (DaoException e) {
            LOGGER.error("Failed to update user " + user, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#delete(Object)}.
     */
    @Override
    public void delete(Long userId) throws ServiceException {
        try {
            userDao.delete(userId);
        } catch (DaoException e) {
            LOGGER.error("Failed to delete user " + userId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.UserService#existsUser(Long)}.
     */
    @Override
    public boolean existsUser(Long userId) throws ServiceException {
        try {
            return userDao.checkUser(userId);
        } catch (DaoException e) {
            LOGGER.error("Failed to create user " + userId, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public User findUser(String login, String password) throws ServiceException {
        try {
            return userDao.getUser(login, password);
        } catch (DaoException e) {
            LOGGER.error("Failed to find user " + login, e);
            throw new ServiceException(e);
        }
    }
}
