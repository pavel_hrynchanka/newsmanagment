package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.domain.to.NewsTO;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * It manages services group and manipulate transfer objects.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsCommonServiceImpl implements NewsCommonService {
    @Autowired
    private NewsService newsService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private CommentService commentService;

    /**
     * Implementation of {@link NewsCommonService#createNewsWithAuthorAndTags(News, List, List)}
     */
    @Override
    public Long createNewsWithAuthorAndTags(News news, List<Long> authorsId, List<Long> tagsId) throws ServiceException {
        Long newsId = newsService.create(news);
        newsService.addAuthorsToNews(news.getNewsId(), (Long[]) authorsId.toArray());
        newsService.addTagsToNews(news.getNewsId(), (Long[]) tagsId.toArray());
        return newsId;
    }

    /**
     * Implementation of {@link NewsCommonService#findAllNewsTO()}
     */
    @Override
    public List<NewsTO> findAllNewsTO() throws ServiceException {
        List<News> newsList = newsService.findAll();
        return buildAllNews(newsList);
    }

    @Override
    public List<NewsTO> findAllNewsTO(int fromNews, int toNews) throws ServiceException {
        List<News> newsList = newsService.findAll(fromNews, toNews);
        return buildAllNews(newsList);
    }

    /**
     * Implementation of {@link NewsCommonService#findNewsTOBySearchCriteria(SearchCriteria)}
     */
    @Override
    public List<NewsTO> findNewsTOBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
        List<News> newsList = newsService.findNewsBySearchCriteria(searchCriteria);
        return buildAllNews(newsList);
    }

    @Override
    public List<NewsTO> findNewsTOBySearchCriteria(SearchCriteria searchCriteria, int fromNews, int toNews) throws ServiceException {
        List<News> newsList = newsService.findNewsBySearchCriteria(searchCriteria, fromNews, toNews);
        return buildAllNews(newsList);
    }

    /**
     * Implementation of {@link NewsCommonService#findAllNewsTO()}
     */
    @Override
    public NewsTO findNewsTOById(Long newsId) throws ServiceException {
        return buildNews(newsId);
    }

    @Override
    public NewsTO findNextNewsTO(Long newsId) throws ServiceException {
        NewsTO newsTO = null;
        News news = newsService.findNextNews(newsId);
        if (news != null) {
            newsTO = buildNews(news.getNewsId());
        }
        return newsTO;
    }

    @Override
    public NewsTO findPreviousNewsTO(Long newsId) throws ServiceException {
        NewsTO newsTO = null;
        News news = newsService.findPreviousNews(newsId);
        if (news != null) {
            newsTO = buildNews(news.getNewsId());
        }
        return newsTO;
    }

    /**
     * Implementation of {@link NewsCommonService#deleteNewsTOById(Long)}
     */
    @Override
    public void deleteNewsTOById(Long newsId) throws ServiceException {
        newsService.deleteTagsFromNews(newsId);
        newsService.deleteAuthorsFromNews(newsId);
        commentService.deleteCommentsFromNews(newsId);
        newsService.delete(newsId);
    }

    private NewsTO buildNews(Long newsId) throws ServiceException {
        NewsTO newsTo = new NewsTO();
        newsTo.setNews(newsService.findById(newsId));
        newsTo.setAuthor(authorService.findNewsAuthors(newsId));
        newsTo.setTagList(tagService.findNewsTags(newsId));
        newsTo.setCommentList(commentService.findNewsComments(newsId));
        return newsTo;
    }

    private List<NewsTO> buildAllNews(List<News> newsList) throws ServiceException {
        List<NewsTO> newsToList = new ArrayList<>();
        for (News news : newsList) {
            newsToList.add(buildNews(news.getNewsId()));
        }
        return newsToList;
    }
}
