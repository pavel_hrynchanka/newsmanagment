package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.AuthorDao;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import com.epam.newsmanagment.service.GenericService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link AuthorService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements AuthorService {
    private final static Logger LOGGER = LogManager.getLogger(AuthorServiceImpl.class);
    @Autowired
    private AuthorDao authorDao;

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#create(Object)}.
     */
    @Override
    public Long create(Author author) throws ServiceException {
        try {
            return authorDao.create(author);
        } catch (DaoException e) {
            LOGGER.error("Failed to create author " + author, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#findById(Object)}.
     */
    @Override
    public Author findById(Long authorId) throws ServiceException {
        try {
            return authorDao.getById(authorId);
        } catch (DaoException e) {
            LOGGER.error("Failed to find author with id " + authorId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findAll()}.
     */
    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return authorDao.getAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to find all authors ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public Long countAll() throws ServiceException {
        try {
            return authorDao.countAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to count all authors ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#update(Object)}.
     */
    @Override
    public void update(Author author) throws ServiceException {
        try {
            authorDao.update(author);
        } catch (DaoException e) {
            LOGGER.error("Failed to update author " + author, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#delete(Object)}.
     */
    @Override
    public void delete(Long authorId) throws ServiceException {
        try {
            authorDao.delete(authorId);
        } catch (DaoException e) {
            LOGGER.error("Failed to delete author with id " + authorId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.AuthorService#findNewsAuthors(Long)}.
     */
    @Override
    public List<Author> findNewsAuthors(Long newsId) throws ServiceException {
        try {
            return authorDao.getAuthorList(newsId);
        } catch (DaoException e) {
            LOGGER.error("Failed to find all authors for news with id  " + newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link AuthorService#findAvailableAuthors()}.
     */
    @Override
    public List<Author> findAvailableAuthors() throws ServiceException {
        try {
            return authorDao.getAvailableAuthorList();
        } catch (DaoException e) {
            LOGGER.error("Failed to find available authors", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.AuthorService#deleteNewsAuthorRelation(Long)}.
     */
    @Override
    public void deleteNewsAuthorRelation(Long authorId) throws ServiceException {
        try {
            authorDao.deleteAuthorNewsRelation(authorId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete relation news author with author's id " + authorId, e);
            throw new ServiceException(e);
        }
    }
}
