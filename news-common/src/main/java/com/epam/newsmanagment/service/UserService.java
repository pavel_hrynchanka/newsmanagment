package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

/**
 * This is service which provide api for operation with {@link com.epam.newsmanagment.dao.UserDao}
 */
public interface UserService extends GenericService<User, Long> {
    /**
     * Ask {@link com.epam.newsmanagment.dao.UserDao} to check if registered user in system.
     *
     * @param userId is an id of checking user.
     * @return true or false according to exists in system.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    boolean existsUser(Long userId) throws ServiceException;

    User findUser(String login, String password) throws ServiceException;
}
