package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.domain.to.NewsTO;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

import java.util.List;

/**
 * It manages services group and manipulate transfer objects.
 */
public interface NewsCommonService {
    /**
     * Create news and add to it authors and tags in one transaction.
     * It calls another services to achieve this goal.
     *
     * @param news      is created news.
     * @param authorsId are authors which will be added to news.
     * @param tagsId    are tags which will be added to news.
     * @return id of created record
     * @throws ServiceException
     */
    Long createNewsWithAuthorAndTags(News news, List<Long> authorsId, List<Long> tagsId) throws ServiceException;

    /**
     * Find all news transfer objects.
     *
     * @return all persisted news transfer objects.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<NewsTO> findAllNewsTO() throws ServiceException;

    List<NewsTO> findAllNewsTO(int fromNews, int toNews) throws ServiceException;

    /**
     * Find news transfer objects according to {@link SearchCriteria}.
     *
     * @param searchCriteria is an parameter for searching.
     * @return news transfers object satisfied to search criteria.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<NewsTO> findNewsTOBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    List<NewsTO> findNewsTOBySearchCriteria(SearchCriteria searchCriteria, int fromNews, int toNews) throws ServiceException;

    /**
     * Find news transfer object by news id.
     *
     * @param newsId is an id of searched news.
     * @return transfer object according to news id.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    NewsTO findNewsTOById(Long newsId) throws ServiceException;

    NewsTO findNextNewsTO(Long newsId) throws ServiceException;

    NewsTO findPreviousNewsTO(Long newsId) throws ServiceException;

    /**
     * Delete news and its components from database.
     *
     * @param newsId is an id of deleted news.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void deleteNewsTOById(Long newsId) throws ServiceException;
}
