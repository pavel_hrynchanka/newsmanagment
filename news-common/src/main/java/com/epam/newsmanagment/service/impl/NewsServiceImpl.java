package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.NewsDao;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.GenericService;
import com.epam.newsmanagment.service.NewsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link NewsService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsServiceImpl implements NewsService {
    private final static Logger LOGGER = LogManager.getLogger(NewsServiceImpl.class);
    @Autowired
    private NewsDao newsDao;

    /**
     * Implementation of {@link GenericService#create(Object)}.
     */
    @Override
    public Long create(News news) throws ServiceException {
        try {
            return newsDao.create(news);
        } catch (DaoException e) {
            LOGGER.error("Can't create news " + news, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findById(Object)}.
     */
    @Override
    public News findById(Long newsId) throws ServiceException {
        try {
            return newsDao.getById(newsId);
        } catch (DaoException e) {
            LOGGER.error("Can't find news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findAll()}.
     */
    @Override
    public List<News> findAll() throws ServiceException {
        try {
            return newsDao.getAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to find all news ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public Long countAll() throws ServiceException {
        try {
            return newsDao.countAll();
        } catch (DaoException e) {
            LOGGER.error("Can't count all news ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#update(Object)}.
     */
    @Override
    public void update(News news) throws ServiceException {
        try {
            newsDao.update(news);
        } catch (DaoException e) {
            LOGGER.error("Can't update news " + news, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#update(Object)}.
     */
    @Override
    public void delete(Long newsId) throws ServiceException {
        try {
            newsDao.delete(newsId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#findNewsBySearchCriteria(SearchCriteria)}.
     */
    @Override
    public List<News> findNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
        try {
            return newsDao.findNews(searchCriteria);
        } catch (DaoException e) {
            LOGGER.error("Can't find news according to search criteria", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findNewsBySearchCriteria(SearchCriteria searchCriteria, int fromNews, int toNews) throws ServiceException {
        try {
            return newsDao.findNews(searchCriteria, fromNews, toNews);
        } catch (DaoException e) {
            LOGGER.error("Can't find news according to search criteria", e);
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> findAll(int fromNews, int toNews) throws ServiceException {
        try {
            return newsDao.getAll(fromNews, toNews);
        } catch (DaoException e) {
            LOGGER.error("Failed to find all news ", e);

            throw new ServiceException(e);
        }
    }

    @Override
    public News findNextNews(Long newsId) throws ServiceException {
        try {
            return newsDao.findNextNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News findPreviousNews(Long newsId) throws ServiceException {
        try {
            return newsDao.findPreviousNews(newsId);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#addAuthorsToNews(Long, Long...)}.
     */
    @Override
    public Long addAuthorsToNews(Long newsId, Long... authorsId) throws ServiceException {
        try {
            return newsDao.addAuthorRelation(newsId, authorsId);
        } catch (DaoException e) {
            LOGGER.error("Can't add authors to news " + authorsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#addTagsToNews(Long, Long...)}.
     */
    @Override
    public Long addTagsToNews(Long newsId, Long... tagsId) throws ServiceException {
        try {
            return newsDao.addTagRelation(newsId, tagsId);
        } catch (DaoException e) {
            LOGGER.error("Can't add tags to news" + tagsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#deleteAuthorsFromNews(Long)}.
     */
    @Override
    public void deleteAuthorsFromNews(Long newsId) throws ServiceException {
        try {
            newsDao.deleteAuthorRelation(newsId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete authors for news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link NewsService#deleteTagsFromNews(Long)}.
     */
    @Override
    public void deleteTagsFromNews(Long newsId) throws ServiceException {
        try {
            newsDao.deleteTagRelation(newsId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete all tags for news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteTagsWireFromNews(Long newsId, Long... tagsId) throws ServiceException {
        try {
            for (int i = 0; i < tagsId.length; i++) {
                newsDao.deleteTagRelation(newsId, tagsId[i]);
            }
        } catch (DaoException e) {
            LOGGER.error("Can't delete some tags for news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }
}
