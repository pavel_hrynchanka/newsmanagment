package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

import java.util.List;

/**
 * This is service which provide api for operation with {@link com.epam.newsmanagment.dao.CommentDao}
 */
public interface CommentService extends GenericService<Comment, Long> {
    /**
     * It asks {@link com.epam.newsmanagment.dao.CommentDao} to find all news comments.
     *
     * @param newsId
     * @return
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<Comment> findNewsComments(Long newsId) throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.CommentDao} to delete all news comments.
     *
     * @param newsId is and id of news for which comments will be deleted.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void deleteCommentsFromNews(Long newsId) throws ServiceException;
}
