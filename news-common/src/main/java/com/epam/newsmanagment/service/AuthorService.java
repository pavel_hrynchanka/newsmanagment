package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

import java.util.List;

/**
 * This is service which provide api for operation with {@link com.epam.newsmanagment.dao.TagDao}
 */
public interface AuthorService extends GenericService<Author, Long> {
    /**
     * It asks {@link com.epam.newsmanagment.dao.TagDao} to find all news authors.
     *
     * @param newsId
     * @return all news authors.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<Author> findNewsAuthors(Long newsId) throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.TagDao} to find all available authors.
     *
     * @return all available authors.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<Author> findAvailableAuthors() throws ServiceException;

    /**
     * It asks {@link com.epam.newsmanagment.dao.TagDao} to authors news.
     *
     *
     * @param authorId is an id of delete author in relation.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void deleteNewsAuthorRelation(Long authorId) throws ServiceException;
}
