package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.TagDao;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.GenericService;
import com.epam.newsmanagment.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link TagService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class TagServiceImpl implements TagService {
    private final static Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);
    @Autowired
    private TagDao tagDao;

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#create(Object)}.
     */
    @Override
    public Long create(Tag tag) throws ServiceException {
        try {
            return tagDao.create(tag);
        } catch (DaoException e) {
            LOGGER.error("Can't create tag " + tag, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#findById(Object)}.
     */
    @Override
    public Tag findById(Long tagId) throws ServiceException {
        try {
            return tagDao.getById(tagId);
        } catch (DaoException e) {
            LOGGER.error("Can't find tag with id " + tagId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findAll()}.
     */
    @Override
    public List<Tag> findAll() throws ServiceException {

        try {
            return tagDao.getAll();
        } catch (DaoException e) {
            LOGGER.error("Can't find all tags ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public Long countAll() throws ServiceException {
        try {
            return tagDao.countAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to count all tags ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public void update(Tag tag) throws ServiceException {
        try {
            tagDao.update(tag);
        } catch (DaoException e) {
            LOGGER.error("Can't update tag " + tag, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#delete(Object)}.
     */
    @Override
    public void delete(Long tagId) throws ServiceException {
        try {
            tagDao.delete(tagId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete tag with id " + tagId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.TagService#findNewsTags(Long)} (Object)}.
     */
    @Override
    public List<Tag> findNewsTags(Long newsId) throws ServiceException {
        try {
            return tagDao.getNewsTagList(newsId);
        } catch (DaoException e) {
            LOGGER.error("Failed to find tags for news " + newsId, e);
            throw new ServiceException(e);
        }
    }
}
