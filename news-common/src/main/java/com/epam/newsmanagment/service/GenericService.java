package com.epam.newsmanagment.service;

import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

import java.util.List;

/**
 * This is service which provide api for dao layer.
 */
public interface GenericService<TO, PK> {
    /**
     * It asks {@link TO} dao for persisting record in database.
     *
     * @param transferObject is an entity which will be created.
     * @return id of created record.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    PK create(TO transferObject) throws ServiceException;

    /**
     * It asks {@link TO} dao for finding entity by id.
     *
     * @param toId is an id of searching entity.
     * @return {@link TO}.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    TO findById(PK toId) throws ServiceException;

    /**
     * It asks {@link TO} dao for finding all records.
     *
     * @return all records od {@link TO}
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<TO> findAll() throws ServiceException;

    /**
     * It asks {@link TO} dao for counting all records.
     *
     * @return quantity of all records.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    Long countAll() throws ServiceException;

    /**
     * It asks {@link TO} dao to update entity.
     *
     * @param transferObject is an entity which will be updated.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void update(TO transferObject) throws ServiceException;

    /**
     * It asks {@link TO} dao for deleting record by id.
     *
     * @param toId is an id of deleted entity.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    void delete(PK toId) throws ServiceException;

}
