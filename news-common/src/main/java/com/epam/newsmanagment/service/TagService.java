package com.epam.newsmanagment.service;

import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;

import java.util.List;

/**
 * This is service which provide api for operation with {@link com.epam.newsmanagment.dao.TagDao}
 */
public interface TagService extends GenericService<Tag, Long> {
    /**
     * Ask {@link com.epam.newsmanagment.dao.TagDao} to find all news tags.
     *
     * @param newsId is an id of searching news.
     * @return all news tags.
     * @throws ServiceException if {@link DaoException} occurs.
     */
    List<Tag> findNewsTags(Long newsId) throws ServiceException;
}
