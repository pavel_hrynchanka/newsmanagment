package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.RoleDao;
import com.epam.newsmanagment.domain.bean.Role;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.GenericService;
import com.epam.newsmanagment.service.RoleService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link RoleService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class RoleServiceImpl implements RoleService {
    private final static Logger LOGGER = LogManager.getLogger(RoleServiceImpl.class);
    @Autowired
    private RoleDao roleDao;

    /**
     * Implementation of {@link GenericService#create(Object)}.
     */
    @Override
    public Long create(Role role) throws ServiceException {
        try {
            return roleDao.create(role);
        } catch (DaoException e) {
            LOGGER.error("Can't create role " + role, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#findById(Object)}.
     */
    @Override
    public Role findById(Long roleId) throws ServiceException {
        try {
            return roleDao.getById(roleId);
        } catch (DaoException e) {
            LOGGER.error("Can't find role with id " + roleId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findAll()}.
     */
    @Override
    public List<Role> findAll() throws ServiceException {
        try {
            return roleDao.getAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to find all roles ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public Long countAll() throws ServiceException {
        try {
            return roleDao.countAll();
        } catch (DaoException e) {
            LOGGER.error("Can't count all roles ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#update(Object)}.
     */
    @Override
    public void update(Role role) throws ServiceException {
        try {
            roleDao.update(role);
        } catch (DaoException e) {
            LOGGER.error("Can't update role " + role, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link com.epam.newsmanagment.service.GenericService#delete(Object)}.
     */
    @Override
    public void delete(Long roleId) throws ServiceException {
        try {
            roleDao.delete(roleId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete role with id " + roleId, e);
            throw new ServiceException(e);
        }
    }
}
