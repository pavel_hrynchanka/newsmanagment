package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.CommentDao;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.CommentService;
import com.epam.newsmanagment.service.GenericService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Implementation of {@link CommentService}.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class CommentServiceImpl implements CommentService {
    private final static Logger LOGGER = LogManager.getLogger(CommentServiceImpl.class);
    @Autowired
    private CommentDao commentDao;

    /**
     * Implementation of {@link GenericService#create(Object)}.
     */
    @Override
    public Long create(Comment comment) throws ServiceException {
        try {
            return commentDao.create(comment);
        } catch (DaoException e) {
            LOGGER.error("Can't create comment " + comment, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findById(Object)}.
     */
    @Override
    public Comment findById(Long roleId) throws ServiceException {
        try {
            return commentDao.getById(roleId);
        } catch (DaoException e) {
            LOGGER.error("Can't find comment with id " + roleId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#findAll()}.
     */
    @Override
    public List<Comment> findAll() throws ServiceException {
        try {
            return commentDao.getAll();
        } catch (DaoException e) {
            LOGGER.error("Failed to find all roles ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#countAll()}.
     */
    @Override
    public Long countAll() throws ServiceException {
        try {
            return commentDao.countAll();
        } catch (DaoException e) {
            LOGGER.error("Can't count all roles ", e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#update(Object)}.
     */
    @Override
    public void update(Comment comment) throws ServiceException {
        try {
            commentDao.update(comment);
        } catch (DaoException e) {
            LOGGER.error("Can't update comment " + comment, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link GenericService#delete(Object)}.
     */
    @Override
    public void delete(Long commentId) throws ServiceException {
        try {
            commentDao.delete(commentId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete comment with id " + commentId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#findNewsComments(Long)}.
     */
    @Override
    public List<Comment> findNewsComments(Long newsId) throws ServiceException {
        try {
            return commentDao.getCommentList(newsId);
        } catch (DaoException e) {
            LOGGER.error("Can't find comment for news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }

    /**
     * Implementation of {@link CommentService#deleteCommentsFromNews(Long)}.
     */
    @Override
    public void deleteCommentsFromNews(Long newsId) throws ServiceException {
        try {
            commentDao.deleteCommentByNews(newsId);
        } catch (DaoException e) {
            LOGGER.error("Can't delete comments for news with id " + newsId, e);
            throw new ServiceException(e);
        }
    }
}
