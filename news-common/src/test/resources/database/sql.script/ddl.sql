--Create tables
CREATE TABLE NM_ROLES(
  ROLE_ID NUMBER(20),
  ROLE_NAME VARCHAR2(50) NOT NULL,
  CONSTRAINT ROLE_ID_PK PRIMARY KEY (ROLE_ID)
);
CREATE TABLE NM_USERS(
  USER_ID NUMBER(20),
  USER_NAME NVARCHAR2(50) NOT NULL,
  USER_LOGIN VARCHAR2(30) NOT NULL,
  USER_PASSWORD VARCHAR2(30) NOT NULL,
  ROLE_ID NUMBER(20) NOT NULL,
  CONSTRAINT USER_ID_PK PRIMARY KEY (USER_ID),
  CONSTRAINT ROLE_ID_FK FOREIGN KEY (ROLE_ID) REFERENCES NM_ROLES(ROLE_ID)
);
CREATE TABLE AUTHORS(
  AUTHOR_ID NUMBER(20),
  AUTHOR_NAME NVARCHAR2(30) NOT NULL,
  EXPIRED TIMESTAMP,
  CONSTRAINT AUTHOR_ID_PK PRIMARY KEY (AUTHOR_ID)
);
CREATE TABLE TAGS(
  TAG_ID NUMBER(20),
  TAG_NAME NVARCHAR2(30) NOT NULL,
  CONSTRAINT TAG_ID_PK PRIMARY KEY(TAG_ID)
);
CREATE TABLE NEWS(
  NEWS_ID NUMBER(20),
  TITLE NVARCHAR2(30) NOT NULL,
  SHORT_TEXT NVARCHAR2(100) NOT NULL,
  FULL_TEXT NVARCHAR2(2000) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  MODIFICATION_DATE DATE NOT NULL,
  CONSTRAINT NEWS_ID_PK PRIMARY KEY (NEWS_ID)
);
CREATE TABLE NEWS_AUTHORS(
  NEWS_ID NUMBER(20) NOT NULL,
  AUTHOR_ID NUMBER(20) NOT NULL,
  CONSTRAINT NEWS_ID_FK FOREIGN KEY(NEWS_ID) REFERENCES NEWS(NEWS_ID),
  CONSTRAINT AUTHOR_ID_FK FOREIGN KEY(AUTHOR_ID)  REFERENCES AUTHORS(AUTHOR_ID)
);
CREATE TABLE NEWS_TAGS(
  NEWS_ID NUMBER(20) NOT NULL,
  TAG_ID NUMBER(20) NOT NULL,
  CONSTRAINT TAGS_NEWS_ID_FK FOREIGN KEY(NEWS_ID) REFERENCES NEWS(NEWS_ID) ON DELETE CASCADE,
  CONSTRAINT TAGS_TAG_ID_FK FOREIGN KEY(TAG_ID) REFERENCES TAGS(TAG_ID) ON DELETE CASCADE
);
CREATE TABLE COMMENTS(
  COMMENT_ID NUMBER(20),
  NEWS_ID NUMBER(20) NOT NULL,
  COMMENT_TEXT NVARCHAR2(100) NOT NULL,
  CREATION_DATE TIMESTAMP NOT NULL,
  CONSTRAINT COMMENT_ID_PK PRIMARY KEY (COMMENT_ID),
  CONSTRAINT COMMENTS_NEWS_ID_FK FOREIGN KEY (NEWS_ID) REFERENCES NEWS(NEWS_ID)
);
--Create sequences
CREATE SEQUENCE NEWS_SEQ
START WITH 1
INCREMENT BY 1
CACHE 50
NOCYCLE;
CREATE SEQUENCE TAG_SEQ
START WITH 1
INCREMENT BY 1
CACHE 50
NOCYCLE;
CREATE SEQUENCE AUTHOR_SEQ
START WITH 1
INCREMENT BY 1
CACHE 50
NOCYCLE;
CREATE SEQUENCE COMMENT_SEQ
START WITH 1
INCREMENT BY 1
CACHE 50
NOCYCLE;
CREATE SEQUENCE USER_SEQ
START WITH 1
INCREMENT BY 1
CACHE 50
NOCYCLE;
CREATE SEQUENCE ROLE_SEQ
START WITH 1
INCREMENT BY 1
CACHE 50
NOCYCLE;