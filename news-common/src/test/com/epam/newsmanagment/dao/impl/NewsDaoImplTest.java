package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.AuthorDao;
import com.epam.newsmanagment.dao.NewsDao;
import com.epam.newsmanagment.dao.TagDao;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Pavel Hrynchanka on 5/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app.context/spring-context-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagment.dao.impl/NewsDaoImplTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com.epam.newsmanagment.dao.impl/NewsDaoImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class NewsDaoImplTest {

    @Autowired
    private NewsDao newsDao;
    @Autowired
    private AuthorDao authorDao;
    @Autowired
    private TagDao tagDao;

    @Test
    public void testCreate() throws DaoException {
        String title = "Sport";
        String shortText = "Event";
        String fullText = "Description";
        String creationDateStr = "2016-05-23 15:11:17";
        String modificationDateStr = "2016-05-25";
        Timestamp creationDate = Timestamp.valueOf(creationDateStr);
        Date modificationDate = Date.valueOf(modificationDateStr);
        News news = new News();
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);
        Long newsId = newsDao.create(news);
        Assert.assertNotNull(newsId);
        news.setNewsId(newsId);
        Assert.assertEquals(news, newsDao.getById(newsId));
    }

    @Test
    public void testGetById() throws DaoException {
        Long newsId = 1L;
        Assert.assertNotNull(newsDao.getById(newsId));
    }

    @Test
    public void testGetAll() throws DaoException {
        List<News> newsList = newsDao.getAll();
        int newsQuantity = 5;
        Assert.assertNotEquals(newsList, Collections.EMPTY_LIST);
        Assert.assertEquals(newsQuantity, newsList.size());
    }

    @Test
    public void testGetAllPaging() throws DaoException {
        int fromRow = 2;
        int toRow = 5;
        List<News> newsList = newsDao.getAll(fromRow, toRow);
        int newsQuantity = 4;
        Assert.assertNotEquals(newsList, Collections.EMPTY_LIST);
        Assert.assertEquals(newsQuantity, newsList.size());
    }

    @Test
    public void testCountAll() throws DaoException {
        Long newsActualQuantity = newsDao.countAll();
        Long newsExpectedQuantity = 5L;
        Assert.assertEquals(newsExpectedQuantity, newsActualQuantity);
    }

    @Test
    public void testUpdate() throws DaoException {
        Long newsId = 1L;
        News news = newsDao.getById(newsId);
        String title = "updTitle";
        String shortText = "updShortText";
        news.setTitle(title);
        news.setShortText(shortText);
        newsDao.update(news);
        Assert.assertEquals(news, newsDao.getById(newsId));

    }

    @Test
    public void testFindNews() throws DaoException {
        SearchCriteria searchCriteria = new SearchCriteria();
        Long authorId = 1L;
        Long tag1Id = 4L;
        Long tag2Id = 6L;
        Author author = authorDao.getById(authorId);
        List<Tag> tagList = new ArrayList<Tag>(Arrays.asList(new Tag[]{tagDao.getById(tag1Id), tagDao.getById(tag2Id)}));
        searchCriteria.setAuthor(author);
        searchCriteria.setTagList(tagList);
        List<News> newsList = newsDao.findNews(searchCriteria);
        //TODO - fix search criteria query
    }

    @Test
    public void testDelete() throws DaoException {
        Long newsId = 1L;
        newsDao.delete(newsId);
        Assert.assertNull(newsDao.getById(newsId));
    }

    @Test
    public void testAddAuthorRelation() throws DaoException {
        Long author1Id = 5L;
        Long author2Id = 6L;
        Long newsId = 1L;
        newsDao.addAuthorRelation(newsId, author1Id, author2Id);
        //TODO fix add relation news_author checking
    }

    @Test
    public void testAddTagRelation() throws DaoException {
        Long tag1Id = 3L;
        Long tag2Id = 5L;
        Long newsId = 1L;
        newsDao.addTagRelation(newsId, tag1Id, tag2Id);
        //TODO fix  add relation news_tag checking
    }

    @Test
    public void testDeleteAuthorRelation() throws DaoException {
        Long newsId = 1L;
        newsDao.deleteAuthorRelation(newsId);
        //TODO fix delete relation news_author checking
    }

    @Test
    public void testDeleteTagsRelation() throws DaoException {
        Long newsId = 1L;
        newsDao.deleteTagRelation(newsId);
        //TODO fix delete relation news_tag checking
    }

    @Test
    public void testDeleteTagRelation() throws DaoException {
        Long newsId = 1L;
        Long tagId = 2L;
        newsDao.deleteTagRelation(newsId,tagId);
    }
}