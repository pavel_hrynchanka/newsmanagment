package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.TagDao;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Collections;
import java.util.List;

/**
 * Created by Pavel Hrynchanka on 5/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app.context/spring-context-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagment.dao.impl/TagDaoImplTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com.epam.newsmanagment.dao.impl/TagDaoImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class TagDaoImplTest {
    @Autowired
    private TagDao tagDao;

    @Test
    public void testCreate() throws DaoException {
        Tag expectedTag = new Tag();
        String tagName = "Rock";
        expectedTag.setTagName(tagName);
        Long actualTagId = tagDao.create(expectedTag);
        Assert.assertNotNull(actualTagId);
        expectedTag.setTagId(actualTagId);
        Tag actualTag = tagDao.getById(actualTagId);
        Assert.assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testGetById() throws DaoException {
        Long tagId = new Long(1);
        String tagName = "Social";
        Tag expectedTagBean = new Tag();
        expectedTagBean.setTagId(tagId);
        expectedTagBean.setTagName(tagName);
        Tag tagBean = tagDao.getById(tagId);
        Assert.assertEquals(expectedTagBean, tagBean);
    }

    @Test
    public void testGetAll() throws DaoException {
        long expectedSize = 6;
        Assert.assertEquals(expectedSize, (long) tagDao.getAll().size());
    }

    @Test
    public void testCountAll() throws DaoException {
        long expectedQuantity = 6;
        Assert.assertEquals(expectedQuantity, (long) tagDao.countAll());
    }

    @Test
    public void testUpdate() throws DaoException {
        Tag expectedTag = new Tag();
        Long tagId = 1L;
        String updTagName = "Culture";
        expectedTag.setTagId(tagId);
        expectedTag.setTagName(updTagName);
        tagDao.update(expectedTag);
        Tag actualTag = tagDao.getById(tagId);
        Assert.assertEquals(expectedTag, actualTag);
    }

    @Test
    public void testDelete() throws DaoException {
        Long tagId = 2L;
        tagDao.delete(tagId);
        Tag actualTag = tagDao.getById(tagId);
        Assert.assertNull(actualTag);
    }

    @Test
    public void testGetTagList() throws DaoException {
        Long newsId = 1l;
        List<Tag> tagList = tagDao.getNewsTagList(newsId);
        Assert.assertNotEquals(tagList, Collections.emptyList());
    }
}