package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.CommentDao;
import com.epam.newsmanagment.dao.NewsDao;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Pavel Hrynchanka on 5/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app.context/spring-context-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagment.dao.impl/CommentDaoImplTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com.epam.newsmanagment.dao.impl/CommentDaoImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class CommentDaoImplTest {

    @Autowired
    private CommentDao commentDao;
    @Autowired
    private NewsDao newsDao;

    @Test
    public void testCreate() throws DaoException {
        Long newsId = 1L;
        News news = newsDao.getById(newsId);
        Comment comment = new Comment();
        String commentText = "good job";
        Timestamp creationDate = new Timestamp(new Date().getTime());
        comment.setCreationDate(creationDate);
        comment.setCommentText(commentText);
        comment.setNews(news);
        Long commentId = commentDao.create(comment);
        Assert.assertNotNull(commentId);
        comment.setCommentId(commentId);
        Comment actualComment = commentDao.getById(commentId);
        Assert.assertEquals(comment, actualComment);
    }

    @Test
    public void testGetById() throws DaoException {
        Long newsId = 1L;
        News news = newsDao.getById(newsId);
        Long commentId = 1L;
        String commentText = "nice";
        String creationDateStr = "2016-05-22 10:10:00";
        Timestamp creationDate = Timestamp.valueOf(creationDateStr);
        Comment expectedComment = new Comment();
        expectedComment.setCommentId(commentId);
        expectedComment.setNews(news);
        expectedComment.setCommentText(commentText);
        expectedComment.setCreationDate(creationDate);
        Comment comment = commentDao.getById(commentId);
        Assert.assertEquals(expectedComment, comment);
    }

    @Test
    public void testGetAll() throws DaoException {
        List<Comment> commentList = commentDao.getAll();
        int expectedSize = 5;
        Assert.assertNotEquals(commentList, Collections.EMPTY_LIST);
        Assert.assertEquals(expectedSize, commentList.size());

    }

    @Test
    public void testCountAll() throws DaoException {
        Long commentQuantity = commentDao.countAll();
        Long expectedSize = 5L;
        Assert.assertEquals(expectedSize, commentQuantity);
    }

    @Test
    public void testUpdate() throws DaoException {
        Long commentId = 2L;
        String commentText = "updated text";
        Comment comment = commentDao.getById(commentId);
        comment.setCommentText(commentText);
        commentDao.update(comment);
        Comment actualComment = commentDao.getById(commentId);
        Assert.assertEquals(comment, actualComment);
    }

    @Test
    public void testDelete() throws DaoException {
        Long commentId = 1L;
        commentDao.delete(commentId);
        Assert.assertNull(commentDao.getById(commentId));
    }

    @Test
    public void testGetCommentList() throws DaoException {
        Long newsId = 1L;
        List<Comment> commentList = commentDao.getCommentList(newsId);
        int commentQuantity = 3;
        Assert.assertNotEquals(commentList, Collections.EMPTY_LIST);
        Assert.assertEquals(commentQuantity, commentList.size());
    }

    @Test
    public void testDeleteCommentByNews() throws DaoException {
        Long newsId = 1L;
        Long expectedQuantity = 2L;
        commentDao.deleteCommentByNews(newsId);
        Assert.assertEquals(expectedQuantity ,commentDao.countAll());
    }
}