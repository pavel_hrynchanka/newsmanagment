package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.UserDao;
import com.epam.newsmanagment.domain.bean.Role;
import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 * Created by Pavel Hrynchanka on 5/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app.context/spring-context-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagment.dao.impl/UserDaoImplTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com.epam.newsmanagment.dao.impl/UserDaoImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class UserDaoImplTest {
    @Autowired
    private UserDao userDao;

    @Test
    public void testCreate() throws DaoException {
        User expectedUser = new User();
        String userName = "nick";
        String userLogin = "nick@gmail.com";
        String userPassword = "nick1";
        expectedUser.setUserName(userName);
        expectedUser.setUserLogin(userLogin);
        expectedUser.setUserPassword(userPassword);
        Role userRole = new Role();
        Long roleId = 1L;
        String roleName = "admin";
        userRole.setRoleId(roleId);
        userRole.setRoleName(roleName);
        expectedUser.setRole(userRole);
        Long actualUserId = userDao.create(expectedUser);
        Assert.assertNotNull(actualUserId);
        expectedUser.setUserId(actualUserId);
        User actualUser = userDao.getById(actualUserId);
        Assert.assertEquals(expectedUser, actualUser);
    }

    @Test
    public void testGetById() throws DaoException {
        User expectedUser = new User();
        Long userId = 2L;
        String userName = "john";
        String userLogin = "john@gmail.com";
        String userPassword = "john1";
        expectedUser.setUserId(userId);
        expectedUser.setUserName(userName);
        expectedUser.setUserLogin(userLogin);
        expectedUser.setUserPassword(userPassword);
        Role userRole = new Role();
        Long roleId = 2L;
        String roleName = "client";
        userRole.setRoleId(roleId);
        userRole.setRoleName(roleName);
        expectedUser.setRole(userRole);
        User actualUser = userDao.getById(userId);
        Assert.assertEquals(expectedUser, actualUser);
    }

    @Test
    public void testGetAll() throws DaoException {
        long expectedSize = 2;
        Assert.assertEquals(expectedSize, (long) userDao.getAll().size());
    }

    @Test
    public void testCountAll() throws DaoException {
        long expectedQuantity = 2;
        Assert.assertEquals(expectedQuantity, (long) userDao.countAll());
    }

    @Test
    public void testUpdate() throws DaoException {
        User expectedUser = new User();
        Long userId = 2L;
        String userName = "john";
        String userLogin = "john@gmail.com";
        String userPassword = "john11";
        expectedUser.setUserId(userId);
        expectedUser.setUserName(userName);
        expectedUser.setUserLogin(userLogin);
        expectedUser.setUserPassword(userPassword);
        Role userRole = new Role();
        Long roleId = 2L;
        String roleName = "client";
        userRole.setRoleId(roleId);
        userRole.setRoleName(roleName);
        expectedUser.setRole(userRole);
        userDao.update(expectedUser);
        Assert.assertEquals(expectedUser, userDao.getById(userId));
    }

    @Test
    public void testDelete() throws DaoException {
        Long userId = 2L;
        userDao.delete(userId);
        Assert.assertNull(userDao.getById(userId));
    }

    @Test
    public void testCheckUser() throws DaoException {
        Long userId = 2L;
        Assert.assertTrue(userDao.checkUser(userId));
    }
}