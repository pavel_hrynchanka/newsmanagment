package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.AuthorDao;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

/**
 * Created by Pavel Hrynchanka on 5/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app.context/spring-context-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagment.dao.impl/AuthorDaoImplTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com.epam.newsmanagment.dao.impl/AuthorDaoImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class AuthorDaoImplTest {
    @Autowired
    private AuthorDao authorDao;

    @Test
    public void testCreate() throws DaoException {
        String authorName = "nickelson";
        Timestamp expired = new Timestamp(1);
        Author author = new Author();
        author.setAuthorName(authorName);
        author.setExpired(expired);
        Long authorId = authorDao.create(author);
        Assert.assertNotNull(authorId);
        author.setAuthorId(authorId);
        Author actualAuthor = authorDao.getById(authorId);
        Assert.assertEquals(author, actualAuthor);
    }

    @Test
    public void testGetById() throws DaoException {
        Author expectedAuthor = new Author();
        Long authorId = 1L;
        String authorName = "smith";
        expectedAuthor.setAuthorId(authorId);
        expectedAuthor.setAuthorName(authorName);
        Assert.assertEquals(expectedAuthor, authorDao.getById(authorId));
    }

    @Test
    public void testGetAll() throws DaoException {
        List<Author> authorList = authorDao.getAll();
        int actualSize = 6;
        Assert.assertNotEquals(authorList, Collections.EMPTY_LIST);
        Assert.assertEquals(authorList.size(), actualSize);
    }

    @Test
    public void testCountAll() throws DaoException {
        Long expectedQuantity = authorDao.countAll();
        Long actualSize = 6L;
        Assert.assertEquals(expectedQuantity, actualSize);
    }

    @Test
    public void testUpdate() throws DaoException {
        Author author = new Author();
        Timestamp expired = new Timestamp(2);
        String authorName = "smith";
        Long authorId = 1L;
        author.setAuthorId(authorId);
        author.setAuthorName(authorName);
        author.setExpired(expired);
        authorDao.update(author);
        Author actualAuthor = authorDao.getById(authorId);
        Assert.assertEquals(author, actualAuthor);
    }

    @Test
    public void testDelete() throws DaoException {
        Long authorId = 1L;
        authorDao.deleteAuthorNewsRelation(authorId);
        authorDao.delete(authorId);
        Author actualAuthor = authorDao.getById(authorId);
        Assert.assertNull(actualAuthor);
    }

    @Test
    public void testGetAuthorList() throws DaoException {
        Long newsId = 1L;
        int authorQuantity = 3;
        List<Author> authorList = authorDao.getAuthorList(newsId);
        Assert.assertNotEquals(authorList, Collections.EMPTY_LIST);
        Assert.assertEquals(authorQuantity, authorList.size());
    }

    @Test
    public void testGetAvailableAuthorList() throws DaoException {
        int authorQuantity = 4;
        List<Author> authorList = authorDao.getAvailableAuthorList();
        Assert.assertNotEquals(authorList, Collections.EMPTY_LIST);
        Assert.assertEquals(authorQuantity, authorList.size());

    }
}