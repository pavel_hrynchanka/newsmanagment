package com.epam.newsmanagment.dao.impl;

import com.epam.newsmanagment.dao.RoleDao;
import com.epam.newsmanagment.domain.bean.Role;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

/**
 * Created by Pavel Hrynchanka on 5/21/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app.context/spring-context-test.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
@DatabaseSetup(value = "classpath:com.epam.newsmanagment.dao.impl/RoleDaoImplTest.xml", type = DatabaseOperation.CLEAN_INSERT)
@DatabaseTearDown(value = "classpath:com.epam.newsmanagment.dao.impl/RoleDaoImplTest.xml", type = DatabaseOperation.DELETE_ALL)
public class RoleDaoImplTest {
    @Autowired
    private RoleDao roleDao;

    @Test
    public void testCreate() throws DaoException {
        Role expectedRole = new Role();
        String roleName = "admin2";
        expectedRole.setRoleName(roleName);
        Long actualRoleId = roleDao.create(expectedRole);
        Assert.assertNotNull(actualRoleId);
        Role actualRole = roleDao.getById(actualRoleId);
        Assert.assertEquals(expectedRole, actualRole);

    }

    @Test
    public void testGetById() throws DaoException {
        Role expectedRole = new Role();
        Long roleId = 1L;
        String roleName = "admin";
        expectedRole.setRoleId(roleId);
        expectedRole.setRoleName(roleName);
        Role actualRole = roleDao.getById(roleId);
        Assert.assertEquals(expectedRole, actualRole);
    }

    @Test
    public void testGetAll() throws DaoException {
        long expectedSize = 2L;
        Assert.assertEquals(expectedSize, (long) roleDao.getAll().size());
    }

    @Test
    public void testCountAll() throws DaoException {
        Long expectedSize = 2L;
        Assert.assertEquals(expectedSize, roleDao.countAll());
    }

    @Test
    public void testUpdate() throws DaoException {
        Role expectedRole = new Role();
        String roleName = "admin2";
        Long roleId = 1L;
        expectedRole.setRoleId(roleId);
        expectedRole.setRoleName(roleName);
        roleDao.update(expectedRole);
        Role actualRole = roleDao.getById(roleId);
        Assert.assertEquals(expectedRole, actualRole);
    }

    @Test
    public void testDelete() throws DaoException {
        Long roleId = 2L;
        roleDao.delete(roleId);
        Assert.assertNull(roleDao.getById(roleId));
    }
}