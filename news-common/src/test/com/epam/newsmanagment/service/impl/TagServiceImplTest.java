package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.TagDao;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/2/2016.
 */
public class TagServiceImplTest {
    @Mock
    private TagDao tagDao;
    @InjectMocks
    private TagServiceImpl tagService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindTagList() throws ServiceException, DaoException {
        List<Tag> tagList = Collections.EMPTY_LIST;
        when(tagDao.getAll()).thenReturn(tagList);
        List<Tag> actualTagList = tagService.findAll();
        Assert.assertEquals(tagList, actualTagList);
        verify(tagDao).getAll();
    }

    @Test
    public void testCreate() throws ServiceException, DaoException {
        Tag tag = new Tag();
        Long tagId = 1L;
        when(tagDao.create(tag)).thenReturn(tagId);
        Long actualTagId = tagService.create(tag);
        Assert.assertEquals(tagId, actualTagId);
        verify(tagDao).create(tag);
    }

    @Test
    public void testFindById() throws ServiceException, DaoException {
        Tag tag = new Tag();
        Long tagId = 3L;
        when(tagDao.getById(tagId)).thenReturn(tag);
        Tag actualTag = tagService.findById(tagId);
        Assert.assertEquals(tag, actualTag);
        verify(tagDao).getById(tagId);
    }

    @Test
    public void testFindAll() throws ServiceException, DaoException {
        List<Tag> tagList = Collections.EMPTY_LIST;
        when(tagDao.getAll()).thenReturn(tagList);
        List<Tag> actualTagList = tagService.findAll();
        Assert.assertEquals(tagList, actualTagList);
        verify(tagDao).getAll();
    }

    @Test
    public void testCountAll() throws ServiceException, DaoException {
        Long quantity = 20L;
        when(tagDao.countAll()).thenReturn(quantity);
        Long actualTagQuantity = tagService.countAll();
        assertEquals(quantity, actualTagQuantity);
        verify(tagDao).countAll();
    }

    @Test
    public void testUpdate() throws ServiceException, DaoException {
        Tag tag = new Tag();
        tagService.update(tag);
        verify(tagDao).update(tag);
    }


    @Test
    public void testDelete() throws ServiceException, DaoException {
        Long tagId = 1L;
        tagService.delete(tagId);
        verify(tagDao).delete(tagId);
    }
}