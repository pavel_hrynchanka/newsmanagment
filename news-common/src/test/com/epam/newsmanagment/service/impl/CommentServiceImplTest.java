package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.CommentDao;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/2/2016.
 */
public class CommentServiceImplTest {
    @Mock
    private CommentDao commentDao;
    @InjectMocks
    private CommentServiceImpl commentService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() throws ServiceException, DaoException {
        Comment comment = new Comment();
        Long commentId = 1L;
        when(commentDao.create(comment)).thenReturn(commentId);
        Long actualCommentId = commentService.create(comment);
        Assert.assertEquals(commentId, actualCommentId);
        verify(commentDao).create(comment);
    }

    @Test
    public void testFindById() throws ServiceException, DaoException {
        Comment comment = new Comment();
        Long commentId = 2L;
        when(commentDao.getById(commentId)).thenReturn(comment);
        Comment actualComment = commentService.findById(commentId);
        Assert.assertEquals(comment, actualComment);
        verify(commentDao).getById(commentId);
    }

    @Test
    public void testFindAll() throws ServiceException, DaoException {
        List<Comment> commentList = Collections.EMPTY_LIST;
        when(commentDao.getAll()).thenReturn(commentList);
        List<Comment> actualCommentList = commentService.findAll();
        Assert.assertEquals(commentList, actualCommentList);
        verify(commentDao).getAll();
    }

    @Test
    public void testCountAll() throws ServiceException, DaoException {
        Long quantity = 2L;
        when(commentDao.countAll()).thenReturn(quantity);
        Long actualQuantity = commentService.countAll();
        Assert.assertEquals(quantity, actualQuantity);
        verify(commentDao).countAll();
    }

    @Test
    public void testUpdate() throws ServiceException, DaoException {
        Comment comment = new Comment();
        commentService.update(comment);
        verify(commentDao).update(comment);
    }

    @Test
    public void testDelete() throws ServiceException, DaoException {
        Long commentId = 2L;
        commentService.delete(commentId);
        verify(commentDao).delete(commentId);
    }

    @Test
    public void testFindCommentsOfNews() throws ServiceException, DaoException {
        List<Comment> commentList = Collections.EMPTY_LIST;
        Long newsId = 1L;
        when(commentDao.getCommentList(newsId)).thenReturn(commentList);
        List<Comment> actualCommentList = commentService.findNewsComments(newsId);
        Assert.assertEquals(commentList, actualCommentList);
        verify(commentDao).getCommentList(newsId);
    }

    @Test
    public void testDeleteCommentsByNewsId() throws ServiceException, DaoException {
        Long newsId = 2L;
        commentService.deleteCommentsFromNews(newsId);
        verify(commentDao).deleteCommentByNews(newsId);
    }
}