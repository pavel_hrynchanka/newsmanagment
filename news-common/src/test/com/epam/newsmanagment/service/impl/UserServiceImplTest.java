package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.UserDao;
import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/2/2016.
 */
public class UserServiceImplTest {
    @Mock
    private UserDao userDao;
    @InjectMocks
    private UserServiceImpl userService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() throws ServiceException, DaoException {
        User user = new User();
        Long userId = 2L;
        when(userDao.create(user)).thenReturn(userId);
        Long actualUserId = userService.create(user);
        Assert.assertEquals(userId, actualUserId);
        verify(userDao).create(user);
    }

    @Test
    public void testFindById() throws ServiceException, DaoException {
        User user = new User();
        Long userId = 1L;
        when(userDao.getById(userId)).thenReturn(user);
        User actualUser = userService.findById(userId);
        Assert.assertEquals(user, actualUser);
        verify(userDao).getById(userId);
    }

    @Test
    public void testFindAll() throws ServiceException, DaoException {
        List<User> userList = Collections.EMPTY_LIST;
        when(userDao.getAll()).thenReturn(userList);
        List<User> actualUserList = userService.findAll();
        Assert.assertEquals(userList, actualUserList);
        verify(userDao).getAll();
    }

    @Test
    public void testCountAll() throws ServiceException, DaoException {
        Long quantity = 10L;
        when(userDao.countAll()).thenReturn(quantity);
        Long actualQuantity = userService.countAll();
        Assert.assertEquals(quantity, actualQuantity);
        verify(userDao).countAll();
    }

    @Test
    public void testUpdate() throws ServiceException, DaoException {
        User user = new User();
        userService.update(user);
        verify(userDao).update(user);
    }

    @Test
    public void testDelete() throws ServiceException, DaoException {
        Long userId = 1L;
        userService.delete(userId);
        verify(userDao).delete(userId);
    }

    @Test
    public void testExistsUser() throws ServiceException, DaoException {
        boolean isExist = true;
        Long userId = 1L;
        when(userDao.checkUser(userId)).thenReturn(isExist);
        boolean actualIsExist = userService.existsUser(userId);
        Assert.assertEquals(isExist, actualIsExist);
        verify(userDao).checkUser(userId);
    }
}