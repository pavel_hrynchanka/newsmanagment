package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.AuthorDao;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/2/2016.
 */
public class AuthorServiceImplTest {
    @Mock
    private AuthorDao authorDao;
    @InjectMocks
    private AuthorServiceImpl authorService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() throws ServiceException, DaoException {
        Author author = new Author();
        Long authorId = 1L;
        when(authorDao.create(author)).thenReturn(authorId);
        Long actualAuthorId = authorService.create(author);
        Assert.assertEquals(authorId, actualAuthorId);
        verify(authorDao).create(author);
    }

    @Test
    public void testFindById() throws ServiceException, DaoException {
        Author author = new Author();
        Long authorId = 2L;
        when(authorDao.getById(authorId)).thenReturn(author);
        Author actualAuthor = authorService.findById(authorId);
        Assert.assertEquals(author, actualAuthor);
        verify(authorDao).getById(authorId);
    }

    @Test
    public void testFindAll() throws ServiceException, DaoException {
        List<Author> authorList = Collections.EMPTY_LIST;
        when(authorDao.getAll()).thenReturn(authorList);
        List<Author> actualAuthorList = authorService.findAll();
        Assert.assertEquals(authorList, actualAuthorList);
        verify(authorDao).getAll();
    }

    @Test
    public void testCountAll() throws ServiceException, DaoException {
        Long quantity = 2L;
        when(authorDao.countAll()).thenReturn(quantity);
        Long actualQuantity = authorService.countAll();
        Assert.assertEquals(quantity, actualQuantity);
        verify(authorDao).countAll();
    }

    @Test
    public void testUpdate() throws ServiceException, DaoException {
        Author author = new Author();
        authorService.update(author);
        verify(authorDao).update(author);
    }

    @Test
    public void testDelete() throws ServiceException, DaoException {
        Long authorId = 2L;
        authorService.delete(authorId);
        verify(authorDao).delete(authorId);
    }

    @Test
    public void testFindAuthorsByNewsId() throws ServiceException, DaoException {
        List<Author> authorList = Collections.EMPTY_LIST;
        Long newsId = 2L;
        when(authorDao.getAuthorList(newsId)).thenReturn(authorList);
        List<Author> actualAuthorList = authorService.findNewsAuthors(newsId);
        Assert.assertEquals(authorList, actualAuthorList);
        verify(authorDao).getAuthorList(newsId);
    }

    @Test
    public void testFindAvailableAuthors() throws ServiceException, DaoException {
        List<Author> authorList = Collections.EMPTY_LIST;
        when(authorDao.getAvailableAuthorList()).thenReturn(authorList);
        List<Author> actualAuthorList = authorService.findAvailableAuthors();
        Assert.assertEquals(authorList, actualAuthorList);
        verify(authorDao).getAvailableAuthorList();
    }

    @Test
    public void testDeleteNewsAuthorRelation() throws ServiceException, DaoException {
        Long authorId = 2L;
        authorService.deleteNewsAuthorRelation(authorId);
        verify(authorDao).deleteAuthorNewsRelation(authorId);
    }
}