package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.domain.to.NewsTO;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/7/2016.
 */
public class NewsCommonServiceImplTest {
    @Mock
    private NewsServiceImpl newsService;
    @Mock
    private AuthorServiceImpl authorService;
    @Mock
    private CommentServiceImpl commentService;
    @Mock
    private TagServiceImpl tagService;

    @InjectMocks
    private NewsCommonServiceImpl newsCommonService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateNewsWithAuthorAndTags() throws ServiceException, DaoException {
        Long newsId = 1L;
        Long[] authorsId = {2L};
        Long[] tagsId = {1L, 2L, 5L};
        Long authorsInserted = 1L;
        Long tagsInserted = 3L;
        News news = new News();
        news.setNewsId(newsId);
        when(newsService.create(news)).thenReturn(newsId);
        when(newsService.addTagsToNews(newsId, tagsId)).thenReturn(tagsInserted);
        when(newsService.addAuthorsToNews(newsId, authorsId)).thenReturn(authorsInserted);
        Long actualNewsId = newsCommonService.createNewsWithAuthorAndTags(news, Arrays.asList(authorsId), Arrays.asList(tagsId));
        Assert.assertEquals(newsId, actualNewsId);
        verify(newsService).create(news);
        verify(newsService).addTagsToNews(newsId, tagsId);
        verify(newsService).addAuthorsToNews(newsId, authorsId);
    }

    @Test
    public void testFindAllNewsTO() throws ServiceException {
        List<NewsTO> newsToList = Collections.EMPTY_LIST;
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsService.findAll()).thenReturn(newsList);
        List<NewsTO> actualNewsToList = newsCommonService.findAllNewsTO();
        Assert.assertEquals(newsToList, actualNewsToList);
        verify(newsService).findAll();
    }

    @Test
    public void testFindAllNewsTOWithPaging() throws ServiceException {
        int fromNews = 2;
        int toNews = 5;
        List<NewsTO> newsToList = Collections.EMPTY_LIST;
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsService.findAll(fromNews, toNews)).thenReturn(newsList);
        List<NewsTO> actualNewsToList = newsCommonService.findAllNewsTO(fromNews, toNews);
        Assert.assertEquals(newsToList, actualNewsToList);
        verify(newsService).findAll(fromNews,toNews);
    }

    @Test
    public void testFindNewsTOBySearchCriteria() throws ServiceException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<NewsTO> newsToList = Collections.EMPTY_LIST;
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsService.findNewsBySearchCriteria(searchCriteria)).thenReturn(newsList);
        List<NewsTO> actualNewsToList = newsCommonService.findNewsTOBySearchCriteria(searchCriteria);
        Assert.assertEquals(newsToList, actualNewsToList);
        verify(newsService).findNewsBySearchCriteria(searchCriteria);
    }

    @Test
    public void testFindNewsTO() throws ServiceException {
        Long newsId = 1L;
        News news = new News();
        List<Author> authorList = Collections.EMPTY_LIST;
        List<Tag> tagList = Collections.EMPTY_LIST;
        List<Comment> commentList = Collections.EMPTY_LIST;
        NewsTO newsTO = new NewsTO();
        newsTO.setNews(news);
        newsTO.setAuthor(authorList);
        newsTO.setTagList(tagList);
        newsTO.setCommentList(commentList);
        when(newsService.findById(newsId)).thenReturn(news);
        when(authorService.findNewsAuthors(newsId)).thenReturn(authorList);
        when(tagService.findNewsTags(newsId)).thenReturn(tagList);
        when(commentService.findNewsComments(newsId)).thenReturn(commentList);
        NewsTO actualNewsTO = newsCommonService.findNewsTOById(newsId);
        Assert.assertEquals(newsTO, actualNewsTO);
        verify(newsService).findById(newsId);
        verify(tagService).findNewsTags(newsId);
        verify(authorService).findNewsAuthors(newsId);
        verify(commentService).findNewsComments(newsId);
    }

    @Test
    public void testDeleteNewsTO() throws ServiceException {
        Long newsId = 1L;
        newsCommonService.deleteNewsTOById(newsId);
        verify(newsService).deleteAuthorsFromNews(newsId);
        verify(newsService).deleteTagsFromNews(newsId);
        verify(commentService).deleteCommentsFromNews(newsId);
        verify(newsService).delete(newsId);
    }
}