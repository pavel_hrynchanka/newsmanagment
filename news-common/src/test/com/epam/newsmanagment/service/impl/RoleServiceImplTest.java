package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.RoleDao;
import com.epam.newsmanagment.domain.bean.Role;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/2/2016.
 */
public class RoleServiceImplTest {
    @Mock
    private RoleDao roleDao;
    @InjectMocks
    private RoleServiceImpl roleService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() throws ServiceException, DaoException {
        Role role = new Role();
        Long roleId = 1L;
        when(roleDao.create(role)).thenReturn(roleId);
        Long actualRoleId = roleService.create(role);
        Assert.assertEquals(roleId, actualRoleId);
        verify(roleDao).create(role);
    }

    @Test
    public void testFindById() throws ServiceException, DaoException {
        Role role = new Role();
        Long roleId = 2L;
        when(roleDao.getById(roleId)).thenReturn(role);
        Role actualRole = roleService.findById(roleId);
        Assert.assertEquals(role, actualRole);
        verify(roleDao).getById(roleId);
    }

    @Test
    public void testFindAll() throws ServiceException, DaoException {
        List<Role> roleList = Collections.EMPTY_LIST;
        when(roleDao.getAll()).thenReturn(roleList);
        List<Role> actualRoleList = roleService.findAll();
        Assert.assertEquals(roleList, actualRoleList);
        verify(roleDao).getAll();
    }

    @Test
    public void testCountAll() throws ServiceException, DaoException {
        Long quantity = 2L;
        when(roleDao.countAll()).thenReturn(quantity);
        Long actualQuantity = roleService.countAll();
        Assert.assertEquals(quantity, actualQuantity);
        verify(roleDao).countAll();
    }

    @Test
    public void testUpdate() throws ServiceException, DaoException {
        Role role = new Role();
        roleService.update(role);
        verify(roleDao).update(role);
    }

    @Test
    public void testDelete() throws ServiceException, DaoException {
        Long roleId = 2L;
        roleService.delete(roleId);
        verify(roleDao).delete(roleId);
    }
}