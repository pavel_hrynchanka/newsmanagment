package com.epam.newsmanagment.service.impl;

import com.epam.newsmanagment.dao.NewsDao;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.exception.dao.DaoException;
import com.epam.newsmanagment.exception.service.ServiceException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Pavel Hrynchanka on 6/2/2016.
 */
public class NewsServiceImplTest {
    @Mock
    private NewsDao newsDao;
    @InjectMocks
    private NewsServiceImpl newsService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreate() throws ServiceException, DaoException {
        News news = new News();
        Long newsId = 1L;
        when(newsDao.create(news)).thenReturn(newsId);
        Long actualNewsId = newsService.create(news);
        Assert.assertEquals(newsId, actualNewsId);
        verify(newsDao).create(news);
    }

    @Test
    public void testFindById() throws ServiceException, DaoException {
        News news = new News();
        Long newsId = 2L;
        when(newsDao.getById(newsId)).thenReturn(news);
        News actualNews = newsService.findById(newsId);
        Assert.assertEquals(news, actualNews);
        verify(newsDao).getById(newsId);
    }

    @Test
    public void testFindAll() throws ServiceException, DaoException {
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsDao.getAll()).thenReturn(newsList);
        List<News> actualNewsList = newsService.findAll();
        Assert.assertEquals(newsList, actualNewsList);
        verify(newsDao).getAll();
    }

    @Test
    public void testFindAllPaging() throws ServiceException, DaoException {
        List<News> newsList = Collections.EMPTY_LIST;
        int fromNews = 2;
        int toNews = 5;
        when(newsDao.getAll(fromNews, toNews)).thenReturn(newsList);
        List<News> actualNewsList = newsService.findAll(fromNews,toNews);
        Assert.assertEquals(newsList, actualNewsList);
        verify(newsDao).getAll(fromNews,toNews);
    }

    @Test
    public void testCountAll() throws ServiceException, DaoException {
        Long quantity = 2L;
        when(newsDao.countAll()).thenReturn(quantity);
        Long actualQuantity = newsService.countAll();
        Assert.assertEquals(quantity, actualQuantity);
        verify(newsDao).countAll();
    }

    @Test
    public void testUpdate() throws ServiceException, DaoException {
        News news = new News();
        newsService.update(news);
        verify(newsDao).update(news);
    }

    @Test
    public void testDelete() throws ServiceException, DaoException {
        Long newsId = 2L;
        newsService.delete(newsId);
        verify(newsDao).delete(newsId);
    }

    @Test
    public void testFindNews() throws ServiceException, DaoException {
        SearchCriteria searchCriteria = new SearchCriteria();
        List<News> newsList = Collections.EMPTY_LIST;
        when(newsDao.findNews(searchCriteria)).thenReturn(newsList);
        List<News> actualNewsList = newsService.findNewsBySearchCriteria(searchCriteria);
        Assert.assertEquals(newsList, actualNewsList);
        verify(newsDao).findNews(searchCriteria);
    }

    @Test
    public void testAddAuthorToNews() throws ServiceException, DaoException {
        Long newsId = 1L;
        Long[] authors = new Long[2];
        Long rowsInserted = 2L;
        when(newsDao.addAuthorRelation(newsId, authors)).thenReturn(rowsInserted);
        Long actualRowsInserted = newsService.addAuthorsToNews(newsId, authors);
        Assert.assertEquals(rowsInserted, actualRowsInserted);
        verify(newsDao).addAuthorRelation(newsId, authors);
    }

    @Test
    public void testAddTagToNews() throws ServiceException, DaoException {
        Long newsId = 1L;
        Long[] tags = new Long[2];
        Long rowsInserted = 2L;
        when(newsDao.addTagRelation(newsId, tags)).thenReturn(rowsInserted);
        Long actualRowsInserted = newsService.addTagsToNews(newsId, tags);
        Assert.assertEquals(rowsInserted, actualRowsInserted);
        verify(newsDao).addTagRelation(newsId, tags);
    }

    @Test
    public void testDeleteAuthorFromNews() throws ServiceException, DaoException {
        Long newsId = 2L;
        newsService.deleteAuthorsFromNews(newsId);
        verify(newsDao).deleteAuthorRelation(newsId);
    }

    @Test
    public void testDeleteTagFromNews() throws ServiceException, DaoException {
        Long newsId = 2L;
        newsService.deleteTagsFromNews(newsId);
        verify(newsDao).deleteTagRelation(newsId);
    }
}