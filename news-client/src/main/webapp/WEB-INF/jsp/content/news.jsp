<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<p><a href="${pageContext.request.contextPath}/news/list">Back</a></p>--%>
<p><a href="${pageContext.request.contextPath}/controller?commandName=showNewsList">Back</a></p>
<section>
    <b>${currentNews.news.title}</b>
    <span>(by ${currentNews.author[0].authorName})</span>
    <span>${currentNews.news.modificationDate}</span>
    <p>
        ${currentNews.news.fullText}
    </p>
    <div>
        <c:forEach items="${currentNews.commentList}" var="comment">
            <form method="post" action="${pageContext.request.contextPath}/controller">
                <span>${comment.creationDate}</span>
                <input type="hidden" name="commentId" value="${comment.commentId}"/>
                <p>${comment.commentText}</p>
                <button type="submit" name="commandName" value="deleteComment">Delete</button>
            </form>
        </c:forEach>
        <form action="${pageContext.request.contextPath}/controller" method="post">
            <textarea name="commentText"></textarea>
            <button name="commandName" value="addComment">Post comment</button>
            <input type="hidden" name="newsId" value="${currentNews.news.newsId}"/>
        </form>
    </div>
</section>
<div style="display: inline-block">
    <c:if test="${previousNews!= null}">
        <form action="${pageContext.request.contextPath}/controller" method="get">
            <button type="submit" name="commandName" value="showNews">Previous</button>
            <input type="hidden" name="newsId" value="${previousNews.news.newsId}"/>
        </form>
    </c:if>
    <c:if test="${nextNews !=null}">
        <form action="${pageContext.request.contextPath}/controller">
            <button type="submit" name="commandName" value="showNews">Next</button>
            <input type="hidden" name="newsId" value="${nextNews.news.newsId}"/>
        </form>
    </c:if>
</div>