<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fnc" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="filter">
    <form method="get" action="${pageContext.request.contextPath}/controller">
        <select name="author" required>
            <c:forEach items="${authorList}" var="author">
                <option value="${author.authorId}">${author.authorName}</option>
            </c:forEach>
        </select>
        <select name="tags" multiple size="1">
            <c:forEach items="${tagList}" var="tag">
                <option value="${tag.tagId}">${tag.tagName}</option>
            </c:forEach>
        </select>
        <button type="submit" name="commandName" value="showNewsListByCriteria">Filter</button>
        <button type="submit" name="commandName" value="resetFilter">Reset</button>
    </form>
</div>
    <c:forEach items="${newsToList}" var="newsTo">
        <section>
            <a href="controller?newsId=${newsTo.news.newsId}&commandName=showNews">${newsTo.news.title}</a>
            <span>(by ${newsTo.author[0].authorName})</span>
            <span>${newsTo.news.modificationDate}</span>
            <p>
                    ${newsTo.news.shortText}
            </p>
            <div class="newsView">
                <ul>
                    <c:forEach items="${newsTo.tagList}" var="tag">
                        <li>#${tag.tagName}</li>
                    </c:forEach>
                </ul>
                <span>Comments(${fnc:length(newsTo.commentList)})</span>
            </div>
        </section>
    </c:forEach>
<ul class="pagination">
    <c:if test="${currentPage!=1 && fnc:length(newsToList)!=0}">
        <a href="${pageContext.request.contextPath}/controller?pageNumber=${currentPage-1}&commandName=showNewsList">Previous</a>
    </c:if>
    <c:forEach begin="1" end="${pageQuantity}" var="i">
        <c:choose>
            <c:when test="${currentPage == i}">
                <li>${i}</li>
            </c:when>
            <c:otherwise>
                <li>
                    <a href="${pageContext.request.contextPath}/controller?pageNumber=${i}&commandName=showNewsList">${i}</a>
                </li>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <c:if test="${currentPage!=pageQuantity && fnc:length(newsToList)!=0}">
        <a href="${pageContext.request.contextPath}/controller?pageNumber=${currentPage+1}&commandName=showNewsList">Next</a>
    </c:if>
</ul>
