<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header class="layout header">
    <h1>News portal - Administration</h1>
    <div>
        <c:if test="${sessionScope.user != null}">
            <span>Hellow, ${sessionScope.user.role.roleName} ${sessionScope.user.userName}</span>
            <form method="get" action="${pageContext.request.contextPath}/controller">
                <button type="submit" name="commandName" value="logout">Logout</button>
            </form>
        </c:if>
    </div>
</header>