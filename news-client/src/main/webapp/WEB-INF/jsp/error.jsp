<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
<p>${pageContext.errorData.requestURI}</p>
<p>${pageContext.errorData.statusCode}</p>
</body>
</html>
