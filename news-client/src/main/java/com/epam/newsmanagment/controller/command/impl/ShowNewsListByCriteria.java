package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.dao.util.PaginationHelper;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.domain.to.NewsTO;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import com.epam.newsmanagment.service.NewsCommonService;
import com.epam.newsmanagment.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Component
public class ShowNewsListByCriteria implements Command {
    private final static String PAGE = "/news/list";
    private final static String AUTHOR_PARAM = "author";
    private final static String TAGS_PARAM = "tags";
    private final static String NEWS_ATTR = "newsToList";
    private final static String PAGE_QUANTITY_ATTR = "pageQuantity";
    private final static String PAGE_NUMBER_PARAM = "pageNumber";
    private final static String CURRENT_PAGE_ATTR = "currentPage";
    private final static Integer RECORDS_PER_PAGE = 3;
    private final static String AUTHORS_ATTR = "authorList";
    private final static String TAGS_ATTR = "tagList";
    private final static String CRITERIA_ATTR = "searchCriteria";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    private Integer pageNumber = 1;
    @Autowired
    private NewsCommonService newsCommonService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;

    @Override

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            HttpSession session = request.getSession(false);
            if (session != null) {
                SearchCriteria searchCriteria = buildSearchCriteria(request);
                buildRequest(request, searchCriteria);
                session.setAttribute(CRITERIA_ATTR, searchCriteria);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PAGE;
    }

    private SearchCriteria buildSearchCriteria(HttpServletRequest request) throws ServiceException {
        SearchCriteria searchCriteria = new SearchCriteria();
        String authorId = request.getParameterValues(AUTHOR_PARAM)[0];
        String[] tags = request.getParameterValues(TAGS_PARAM);
        Author author = null;
        List<Tag> tagList = new ArrayList<>();
        if (tags != null) {
            author = buildAuthor(authorId);
            tagList = buildTagList(tags);
        } else {
            author = buildAuthor(authorId);
        }
        searchCriteria.setAuthor(author);
        searchCriteria.setTagList(tagList);
        return searchCriteria;
    }

    private Author buildAuthor(String authorId) throws ServiceException {
        return authorService.findById(Long.valueOf(authorId));
    }

    private List<Tag> buildTagList(String... tags) throws ServiceException {
        List<Tag> tagList = null;
        if (tags != null) {
            tagList = new ArrayList<>();
            for (int i = 0; i < tags.length; ++i) {
                Tag tag = tagService.findById(Long.valueOf(tags[i]));
                tagList.add(tag);
            }
            return tagList;
        } else {
            return tagList;
        }
    }

    private void buildRequest(HttpServletRequest request, SearchCriteria searchCriteria) throws ServiceException {
        int recordsQuantity = newsCommonService.findNewsTOBySearchCriteria(searchCriteria).size();
        PaginationHelper paginator = new PaginationHelper(recordsQuantity, RECORDS_PER_PAGE);
        if (request.getParameter(PAGE_NUMBER_PARAM) != null) {
            pageNumber = Integer.valueOf(request.getParameter(PAGE_NUMBER_PARAM));
        }
        request.setAttribute(PAGE_QUANTITY_ATTR, paginator.countPages());
        request.setAttribute(CURRENT_PAGE_ATTR, pageNumber);
        List<NewsTO> newsTOList = newsCommonService.findNewsTOBySearchCriteria(searchCriteria, paginator.getFromBound(pageNumber), paginator.getToBound(pageNumber));
        request.setAttribute(NEWS_ATTR, newsTOList);
        request.setAttribute(AUTHORS_ATTR, authorService.findAvailableAuthors());
        request.setAttribute(TAGS_ATTR, tagService.findAll());
        request.setAttribute(REPLY_ATTR, FWD_ATTR);
    }
}
