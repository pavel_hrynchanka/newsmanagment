package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class DeleteComment implements Command {
    private final static String COMMENT_ID_PARAM = "commentId";
    private final static String PAGE = "/controller?commandName=showNewsList";
    @Autowired
    private CommentService commentService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Long commentId = Long.valueOf(request.getParameter(COMMENT_ID_PARAM));
        try {
            commentService.delete(commentId);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }
}
