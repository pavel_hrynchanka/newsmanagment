package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.Comment;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.CommentService;
import com.epam.newsmanagment.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Component
public class AddComment implements Command {
    private final static String PAGE = "/controller?commandName=showNewsList";
    private final static String NEWS_ID_PARAM = "newsId";
    private final static String TEXT_PARAM = "commentText";
    @Autowired
    private CommentService commentService;
    @Autowired
    private NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Long tagId = Long.valueOf(request.getParameter(NEWS_ID_PARAM));
        String commentText = request.getParameter(TEXT_PARAM);
        try {
            Timestamp creationDate = Timestamp.valueOf(LocalDateTime.now());
            News news = newsService.findById(tagId);
            Comment comment = new Comment();
            comment.setNews(news);
            comment.setCommentText(commentText);
            comment.setCreationDate(creationDate);
            commentService.create(comment);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }
}
