package com.epam.newsmanagment.controller;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class FrontController extends HttpServlet {
    private final static Logger LOGGER = LogManager.getLogger(FrontController.class);
    private final static String ERROR_PAGE = "/error";
    private final static String COMMAND_PARAM = "commandName";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    private WebApplicationContext webApplicationContext;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        ServletContext servletContext = this.getServletContext();
        webApplicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String commandName = req.getParameter(COMMAND_PARAM);
        Command command = (Command) webApplicationContext.getBean(commandName);
        try {
            final String page = command.execute(req, resp);
            final String responseType = (String) req.getAttribute(REPLY_ATTR);
            if ((responseType != null) && (responseType.equals(FWD_ATTR))) {
                req.getRequestDispatcher(page).forward(req, resp);
            } else {
                resp.sendRedirect(page);
            }
        } catch (CommandException e) {
            LOGGER.debug(e);
            resp.sendRedirect(ERROR_PAGE);
        }
    }
}
