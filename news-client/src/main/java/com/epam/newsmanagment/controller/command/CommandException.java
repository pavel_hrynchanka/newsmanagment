package com.epam.newsmanagment.controller.command;

/**
 * Created by Pavel Hrynchanka on 6/16/2016.
 */
public class CommandException extends Exception {
    public CommandException(Throwable cause) {
        super(cause);
    }
}
