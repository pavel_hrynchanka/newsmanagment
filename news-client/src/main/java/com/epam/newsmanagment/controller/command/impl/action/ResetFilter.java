package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class ResetFilter implements Command {
    private final static String CRITERIA_ATTR = "searchCriteria";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    private final static String PAGE = "/controller?commandName=showNewsList";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.setAttribute(CRITERIA_ATTR, null);
            request.setAttribute(REPLY_ATTR, FWD_ATTR);
        }
        return PAGE;
    }
}
