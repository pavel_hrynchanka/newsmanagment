package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.domain.to.NewsTO;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.NewsCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class ShowNews implements Command {
    private final static String PAGE = "/news";
    private final static String NEWS_ID_PARAM = "newsId";
    private final static String NEXT_NEWS_ATTR = "nextNews";
    private final static String PREV_NEWS_ATTR = "previousNews";
    private final static String CURRENT_NEWS_ATTR = "currentNews";
    private final static String CRITERIA_ATTR = "searchCriteria";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    @Autowired
    private NewsCommonService newsCommonService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Long newsId = Long.valueOf(request.getParameter(NEWS_ID_PARAM));
        HttpSession session = request.getSession(false);
        try {
            NewsTO newsTo = newsCommonService.findNewsTOById(newsId);
            NewsTO nextNews = null;
            NewsTO prevNews = null;
            if (session != null && session.getAttribute(CRITERIA_ATTR) != null) {
                SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(CRITERIA_ATTR);
                List<NewsTO> newsTOList = newsCommonService.findNewsTOBySearchCriteria(searchCriteria);
                int currentIndex = newsTOList.indexOf(newsTo);
                if (currentIndex != newsTOList.size() - 1) {
                    nextNews = newsTOList.get(currentIndex + 1);
                }
                if (currentIndex != 0) {
                    prevNews = newsTOList.get(currentIndex - 1);
                }
            } else {
                nextNews = newsCommonService.findNextNewsTO(newsId);
                prevNews = newsCommonService.findPreviousNewsTO(newsId);
            }
            request.setAttribute(CURRENT_NEWS_ATTR, newsTo);
            request.setAttribute(NEXT_NEWS_ATTR, nextNews);
            request.setAttribute(PREV_NEWS_ATTR, prevNews);
            request.setAttribute(REPLY_ATTR, FWD_ATTR);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PAGE;
    }
}
