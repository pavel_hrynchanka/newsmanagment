# News Managment

> Application perfoms simple system for news managment.


## Branch managment

> **master** contains stable version of completed task(actual task2-servlet-version).

> **dev** contains developed version of the project(actual task1 & task2 in progress).

## Task1

> Persistence and Service layer

###Requirements

> Database : Oracle (version 10 and higher).

> Data access : JDBC (only SQL).

> Spring DI and IO.

## Task2

> UI for Task1

### Requirements
>  JSP&Servlet

>  **Without Spring MVC. After servlet version int's necessary to use Spring MVC**

>  JavaScript(JQuery)