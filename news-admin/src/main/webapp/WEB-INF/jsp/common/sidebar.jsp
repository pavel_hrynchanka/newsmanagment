<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="sidebar">
    <form action="${pageContext.request.contextPath}/controller" method="GET">
        <ul class="menuButton">
            <li>
                <button type="submit" name="commandName" value="showNewsList">News List</button>
            </li>
            <li>
                <button type="submit" name="commandName" value="showAddNews">Add News</button>
            </li>
            <li>
                <button type="submit" name="commandName" value="showUpdAuthors">sAdd/Update
                    Author
                </button>
            </li>
            <li>
                <button type="submit" name="commandName" value="showUpdTags">Add/Update Tags</button>
            </li>
        </ul>
    </form>
</div>