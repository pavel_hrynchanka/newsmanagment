<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${authorList}" var="author">
    <form class="edit-entity" method="POST" action="${pageContext.request.contextPath}/controller">
        <div>
            <span>Author: </span>
            <input type="text" name="authorName" value="${author.authorName}">
            <input type="hidden" name="authorId" value="${author.authorId}">
            <button hidden type="button">Edit</button>
            <button type="submit" name="commandName" value="updateAuthor">Update</button>
            <c:if test="${author.expired eq null}">
                <input type="radio" name="isExpired">Expire</input>
            </c:if>
            <button hidden type="button">Cancel</button>
        </div>
    </form>
</c:forEach>
<form method="POST" action="${pageContext.request.contextPath}/controller">
    <span>Add author</span>
    <input name="authorName" type="text">
    <button type="submit" name="commandName" value="addAuthor">Save</button>
</form>
