<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="content">
    <form action="${pageContext.request.contextPath}/controller" method="post" class="news-form">
        <p><span>Title</span><input type="text" name="title" required/></p>
        <p><span>Brief</span><textarea name="brief" required></textarea></p>
        <p><span>Content</span><textarea name="content" required></textarea></p>
        <div>
            <select name="author" required>
                <c:forEach items="${authorList}" var="author">
                    <option value="${author.authorId}">${author.authorName}</option>
                </c:forEach>
            </select>
            <select name="tags" multiple size="3">
                <c:forEach items="${tagList}" var="tag">
                    <option value="${tag.tagId}">${tag.tagName}</option>
                </c:forEach>
            </select>
            <button type="submit" name=commandName value="addNews">Save</button>
        </div>
    </form>
</div>
</div>