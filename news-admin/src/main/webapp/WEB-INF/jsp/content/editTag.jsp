<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:forEach items="${tagList}" var="tag">
    <form class="edit-entity" method="POST" action="${pageContext.request.contextPath}/controller">
        <div>
            <span>Tag: </span>
            <input type="text" name="tagName" value="${tag.tagName}">
            <input type="hidden" name="tagId" value="${tag.tagId}">
            <button hidden type="button">Edit</button>
            <button type="submit" name="commandName" value="updateTag">Update</button>
            <button type="submit" name="commandName" value="deleteTag">Delete</button>
            <button hidden type="button">Cancel</button>
        </div>
    </form>
</c:forEach>
<form method="POST" action="${pageContext.request.contextPath}/controller">
    <span>Add tag</span>
    <input name="tagName" type="text">
    <button type="submit" name="commandName" value="addTag">Save</button>
</form>