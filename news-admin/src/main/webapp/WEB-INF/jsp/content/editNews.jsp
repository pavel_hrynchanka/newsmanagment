<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form action="${pageContext.request.contextPath}/controller" method="post">
    <span>Title</span><input type="text" name="title" value="${news.title}"/>
    <p>
    <textarea name="brief" rows="10">
        ${news.shortText}
    </textarea>
    </p>
    <p>
    <textarea name="content" rows="10">
        ${news.fullText}
    </textarea>
    </p>
    <p>Choose deleted tags</p>
    <select name="tagsForDelete" multiple size="3">
        <c:forEach items="${newsTagList}" var="tag">
            <option value="${tag.tagId}">${tag.tagName}</option>
        </c:forEach>
    </select>
    <p>Choose added tags</p>
    <select name="tagsForAdd" multiple size="3">
        <c:forEach items="${tagList}" var="tag">
            <option value="${tag.tagId}">${tag.tagName}</option>
        </c:forEach>
    </select>
    <button type="submit" name="commandName" value="updateNews">Update</button>
    <input type="hidden" name="newsId" value="${news.newsId}">
</form>