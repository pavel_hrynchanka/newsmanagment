<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html>
<head>
    <title><tiles:getAsString name="title"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
</head>
<body>
<tiles:insertAttribute name="header"/>
<div class="layout">
    <tiles:insertAttribute name="sidebar"/>
    <div class="content">
        <tiles:insertAttribute name="content"/>
    </div>
</div>
<tiles:insertAttribute name="footer"/>
</body>
</html>
