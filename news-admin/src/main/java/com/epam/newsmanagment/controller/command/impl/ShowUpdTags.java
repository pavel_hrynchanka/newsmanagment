package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class ShowUpdTags implements Command {
    private final static String TAG_ATTR = "tagList";
    private final static String PAGE = "/edit/tag";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    @Autowired
    private TagService tagService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            List<Tag> tagList = tagService.findAll();
            request.setAttribute(TAG_ATTR, tagList);
            request.setAttribute(REPLY_ATTR, FWD_ATTR);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PAGE;
    }
}
