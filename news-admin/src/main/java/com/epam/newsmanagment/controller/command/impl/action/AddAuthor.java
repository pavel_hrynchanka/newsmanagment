package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AddAuthor implements Command {
    private final static String AUTHOR_NAME_PARAM = "authorName";
    private final static String PAGE = "/controller?commandName=showUpdAuthors";
    @Autowired
    private AuthorService authorService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String authorName = request.getParameter(AUTHOR_NAME_PARAM);
        Author author = new Author();
        author.setAuthorName(authorName);
        try {
            authorService.create(author);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }
}
