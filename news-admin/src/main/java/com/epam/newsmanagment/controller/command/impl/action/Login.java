package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.User;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class Login implements Command {
    private final static String PAGE = "/controller?commandName=showNewsList";
    private final static String LOGIN_PARAM = "login";
    private final static String PWD_PARAM = "password";
    private final static String USER_ATTR = "user";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    @Autowired
    private UserService userService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String login = request.getParameter(LOGIN_PARAM);
        String password = request.getParameter(PWD_PARAM);
        try {
            User user = userService.findUser(login, password);
            if (user != null) {
                HttpSession session = request.getSession();
                session.setAttribute(USER_ATTR, user);
                request.setAttribute(REPLY_ATTR,FWD_ATTR);
                return PAGE;
            } else {
                return request.getContextPath();
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
