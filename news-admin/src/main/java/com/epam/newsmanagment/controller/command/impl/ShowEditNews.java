package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.NewsService;
import com.epam.newsmanagment.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class ShowEditNews implements Command {
    private final static String PAGE = "/edit/news";
    private final static String NEWS_ID_PARAM = "newsId";
    private final static String TAG_LIST_ATTR = "tagList";
    private final static String NEWS_TAG_LIST_ATTR = "newsTagList";
    private final static String NEWS_ATTR = "news";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    @Autowired
    private TagService tagService;
    @Autowired
    private NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Long newsId = Long.valueOf(request.getParameter(NEWS_ID_PARAM));
        try {
            List<Tag> newsTagList = tagService.findNewsTags(newsId);
            List<Tag> tagList = tagService.findAll();
            tagList.removeAll(newsTagList);
            request.setAttribute(TAG_LIST_ATTR, tagList);
            request.setAttribute(NEWS_TAG_LIST_ATTR, newsTagList);
            request.setAttribute(NEWS_ATTR, newsService.findById(newsId));
            request.setAttribute(REPLY_ATTR, FWD_ATTR);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PAGE;
    }
}
