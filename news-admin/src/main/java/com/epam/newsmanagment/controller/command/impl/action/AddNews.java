package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.NewsCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@Component
public class AddNews implements Command {
    private final static String PAGE = "/controller?commandName=showNewsList";
    private final static String AUTHOR_PARAM = "author";
    private final static String TAG_PARAM = "tags";
    private final static String TITLE_PARAM = "title";
    private final static String BRIEF_PARAM = "brief";
    private final static String CONTENT_PARAM = "content";
    @Autowired
    private NewsCommonService newsCommonService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        News news = buildNews(request);
        Long[] authorsId = toLongArray(request.getParameterValues(AUTHOR_PARAM));
        Long[] tagsId = toLongArray(request.getParameterValues(TAG_PARAM));
        List<Long> authorList = Arrays.asList(authorsId);
        List<Long> tagList = Arrays.asList(tagsId);
        try {
            newsCommonService.createNewsWithAuthorAndTags(news, authorList, tagList);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }

    private News buildNews(HttpServletRequest request) {
        News news = new News();

        String title = request.getParameter(TITLE_PARAM);
        LocalDateTime dateTime = LocalDateTime.now();
        Timestamp creationDate = Timestamp.valueOf(dateTime);
        java.sql.Date modificationDate = java.sql.Date.valueOf(dateTime.toLocalDate());
        String brief = request.getParameter(BRIEF_PARAM);
        String content = request.getParameter(CONTENT_PARAM);

        news.setTitle(title);
        news.setShortText(brief);
        news.setFullText(content);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);

        return news;
    }

    private Long[] toLongArray(String... stringArray) {
        Long[] longArray = new Long[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            longArray[i] = Long.valueOf(stringArray[i]);
        }
        return longArray;
    }
}
