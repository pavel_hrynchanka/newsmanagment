package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.Tag;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UpdateTag implements Command {
    private final static String TAG_ID_PARAM = "tagId";
    private final static String TAG_NAME_PARAM = "tagName";
    private final static String PAGE = "/controller?commandName=showUpdTags";
    @Autowired
    private TagService tagService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Long tagId = Long.valueOf(request.getParameter(TAG_ID_PARAM));
        String tagName = request.getParameter(TAG_NAME_PARAM);
        try {
            Tag tag = tagService.findById(tagId);
            tag.setTagName(tagName);
            tagService.update(tag);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }
}
