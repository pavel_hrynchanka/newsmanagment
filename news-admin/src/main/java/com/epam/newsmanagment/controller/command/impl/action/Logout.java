package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class Logout implements Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return request.getContextPath();
    }
}
