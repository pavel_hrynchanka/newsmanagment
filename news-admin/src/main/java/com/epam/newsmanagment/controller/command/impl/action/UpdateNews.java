package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.News;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;

@Component
public class UpdateNews implements Command {
    private final static String PAGE = "/controller?commandName=showNewsList";
    private final static String NEWS_ID_PARAM = "newsId";
    private final static String TITLE_PARAM = "title";
    private final static String BRIEF_PARAM = "brief";
    private final static String CONTENT_PARAM = "content";
    private final static String ADD_TAGS_PARAM = "tagsForAdd";
    private final static String DELETE_TAGS_PARAM = "tagsForDelete";
    @Autowired
    private NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            Long newsId = Long.valueOf(request.getParameter(NEWS_ID_PARAM));
            News news = newsService.findById(newsId);
            newsService.update(setAllParameters(news, request));
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }

    private News setAllParameters(News news, HttpServletRequest request) throws ServiceException {
        String title = request.getParameter(TITLE_PARAM);
        String shortText = request.getParameter(BRIEF_PARAM);
        String fullText = request.getParameter(CONTENT_PARAM);
        String[] tagsAddId = request.getParameterValues(ADD_TAGS_PARAM);
        String[] tagsDeleteId = request.getParameterValues(DELETE_TAGS_PARAM);
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setModificationDate(Date.valueOf(LocalDate.now()));
        if (tagsDeleteId != null) {
            System.out.println(Arrays.toString(tagsDeleteId));
            newsService.deleteTagsWireFromNews(news.getNewsId(), getLongTagId(tagsDeleteId));
        }
        if (tagsAddId != null) {
            System.out.println(Arrays.toString(tagsAddId));
            newsService.addTagsToNews(news.getNewsId(), getLongTagId(tagsAddId));

        }
        return news;
    }

    private Long[] getLongTagId(String... stringTagId) {
        Long[] longTagId = new Long[stringTagId.length];
        for (int i = 0; i < stringTagId.length; i++) {
            longTagId[i] = Long.valueOf(stringTagId[i]);
        }
        return longTagId;
    }
}

