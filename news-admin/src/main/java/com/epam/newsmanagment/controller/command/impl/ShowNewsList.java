package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.dao.util.PaginationHelper;
import com.epam.newsmanagment.domain.criteria.SearchCriteria;
import com.epam.newsmanagment.domain.to.NewsTO;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import com.epam.newsmanagment.service.NewsCommonService;
import com.epam.newsmanagment.service.NewsService;
import com.epam.newsmanagment.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Component
public class ShowNewsList implements Command {
    private final static String PAGE = "/news/list";
    private final static Integer RECORDS_PER_PAGE = 3;
    private final static String PAGE_NUMBER_PARAM = "pageNumber";
    private final static String NEWS_ATTR = "newsToList";
    private final static String AUTHORS_ATTR = "authorList";
    private final static String TAGS_ATTR = "tagList";
    private final static String CURRENT_PAGE_ATTR = "currentPage";
    private final static String PAGE_QUANTITY_ATTR = "pageQuantity";
    private final static String CRITERIA_ATTR = "searchCriteria";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    private Integer pageNumber = 1;
    @Autowired
    private NewsCommonService newsCommonService;
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;
    @Autowired
    private NewsService newsService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute(CRITERIA_ATTR) != null) {
                SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(CRITERIA_ATTR);
                buildRequest(request, searchCriteria);
            } else {
                buildRequest(request);
            }
            return PAGE;
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }

    private void buildRequest(HttpServletRequest request) throws ServiceException {
        PaginationHelper paginator = new PaginationHelper(newsService.countAll().intValue(), RECORDS_PER_PAGE);
        if (request.getParameter(PAGE_NUMBER_PARAM) != null) {
            pageNumber = Integer.valueOf(request.getParameter(PAGE_NUMBER_PARAM));
        }
        request.setAttribute(PAGE_QUANTITY_ATTR, paginator.countPages());
        request.setAttribute(CURRENT_PAGE_ATTR, pageNumber);
        request.setAttribute(NEWS_ATTR, newsCommonService.findAllNewsTO(paginator.getFromBound(pageNumber), paginator.getToBound(pageNumber)));
        request.setAttribute(AUTHORS_ATTR, authorService.findAvailableAuthors());
        request.setAttribute(TAGS_ATTR, tagService.findAll());
        request.setAttribute(REPLY_ATTR, FWD_ATTR);
    }

    private void buildRequest(HttpServletRequest request, SearchCriteria searchCriteria) throws ServiceException {
        int recordsQuantity = newsCommonService.findNewsTOBySearchCriteria(searchCriteria).size();
        PaginationHelper paginator = new PaginationHelper(recordsQuantity, RECORDS_PER_PAGE);
        if (request.getParameter(PAGE_NUMBER_PARAM) != null) {
            pageNumber = Integer.valueOf(request.getParameter(PAGE_NUMBER_PARAM));
        }
        request.setAttribute(PAGE_QUANTITY_ATTR, paginator.countPages());
        request.setAttribute(CURRENT_PAGE_ATTR, pageNumber);
        List<NewsTO> newsTOList = newsCommonService.findNewsTOBySearchCriteria(searchCriteria, paginator.getFromBound(pageNumber), paginator.getToBound(pageNumber));
        request.setAttribute(NEWS_ATTR, newsTOList);
        request.setAttribute(AUTHORS_ATTR, authorService.findAvailableAuthors());
        request.setAttribute(TAGS_ATTR, tagService.findAll());
        request.setAttribute(REPLY_ATTR, FWD_ATTR);
    }
}
