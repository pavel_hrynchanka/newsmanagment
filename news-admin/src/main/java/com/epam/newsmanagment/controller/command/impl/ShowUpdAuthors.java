package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class ShowUpdAuthors implements Command {
    private final static String AUTHOR_ATTR = "authorList";
    private final static String PAGE = "/edit/author";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    @Autowired
    private AuthorService authorService;


    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            request.setAttribute(AUTHOR_ATTR, authorService.findAll());
            request.setAttribute(REPLY_ATTR, FWD_ATTR);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PAGE;
    }
}
