package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.NewsCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class DeleteNews implements Command {
    private final static String DELETED_NEWS_PARAM = "deletedNews";
    private final static String PAGE = "/controller?commandName=showNewsList";
    @Autowired
    private NewsCommonService newsCommonService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String[] checked = request.getParameterValues(DELETED_NEWS_PARAM);
        try {
            if (checked != null) {
                //TODO fix page number after deleting all news per page
                deleteNewsTo(checked);
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }

    private void deleteNewsTo(String... newsIdArray) throws ServiceException {
        for (int i = 0; i < newsIdArray.length; ++i) {
            newsCommonService.deleteNewsTOById(Long.valueOf(newsIdArray[i]));
        }
    }
}
