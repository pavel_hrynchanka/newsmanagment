package com.epam.newsmanagment.controller.command.impl;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import com.epam.newsmanagment.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class ShowAddNews implements Command {
    private final static String PAGE = "/add/news";
    private final static String AUTHOR_ATTR = "authorList";
    private final static String TAG_ATTR = "tagList";
    private final static String REPLY_ATTR = "reply";
    private final static String FWD_ATTR = "forward";
    @Autowired
    private AuthorService authorService;
    @Autowired
    private TagService tagService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        try {
            request.setAttribute(AUTHOR_ATTR, authorService.findAll());
            request.setAttribute(TAG_ATTR, tagService.findAll());
            request.setAttribute(REPLY_ATTR, FWD_ATTR);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PAGE;
    }
}
