package com.epam.newsmanagment.controller.command.impl.action;

import com.epam.newsmanagment.controller.command.Command;
import com.epam.newsmanagment.controller.command.CommandException;
import com.epam.newsmanagment.domain.bean.Author;
import com.epam.newsmanagment.exception.service.ServiceException;
import com.epam.newsmanagment.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Component
public class UpdateAuthor implements Command {
    private final static String AUTHOR_ID_PARAM = "authorId";
    private final static String AUTHOR_NAME_PARAM = "authorName";
    private final static String EXPIRED_PARAM = "isExpired";
    private final static String PAGE = "/controller?commandName=showUpdAuthors";
    @Autowired
    private AuthorService authorService;

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        Long authorId = Long.valueOf(request.getParameter(AUTHOR_ID_PARAM));
        String authorName = request.getParameter(AUTHOR_NAME_PARAM);
        String isExpired = request.getParameter(EXPIRED_PARAM);
        try {
            Author author = authorService.findById(authorId);
            author.setAuthorName(authorName);
            if (author.getExpired() == null && (isExpired != null)) {
                author.setExpired(Timestamp.valueOf(LocalDateTime.now()));
            }
            authorService.update(author);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return request.getContextPath() + PAGE;
    }
}
